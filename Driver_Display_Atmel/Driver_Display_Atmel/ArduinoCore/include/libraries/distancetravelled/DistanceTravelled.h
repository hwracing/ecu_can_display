#ifndef _DISTANCETRAVELLED_h
#define _DISTANCETRAVELLED_h


#define DISTANCE_TICKS_ADDRESS    0x10
#define DISTANCE_OVERFLOW_ADDRESS 0x30

#define WHEEL_CIRCUMFERENCE            163 //cm 20.5"*25.4*3.14/10 
#define DISTANCE_PER_TICK              41  //cm
#define OVERFLOW_DISTANCE              2686935 //cm

#include "EEPROM.h"

class DistanceTravelled
{
  public:
  DistanceTravelled(uint8_t dummy_value);
  void init();
  void reset();
  void writeTicks(uint16_t ticks);
  void writeOverflows(uint16_t overflows);
  uint16_t readEEPROMTicks();
  double getDistanceKM();
  uint32_t getDistanceCM();
  uint16_t getDistanceKMInteger();
  uint16_t readEEPROMOverflows();
  void resetEEPROM();
  void writeDistance();

  private:
  uint16_t readTimer5();
  void loadTimer5(uint16_t ticks);
  void setupTimer5();
  void startTimer5();
  void stopTimer5();


};

#endif