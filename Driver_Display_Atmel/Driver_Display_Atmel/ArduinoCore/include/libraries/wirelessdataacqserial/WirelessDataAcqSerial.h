/*WirelessDataAcqSerial.h
/----------------------------------------------------------------------------
//(C) COPYRIGHT 2018 HWRacing Formula Student Team
//              ALL RIGHTS RESERVED
// Author : Bruce Thomson
//----------------------------------------------------------------------------
// Purpose: Sends ECU CAN data in the format below to a data acqusition system 
//          and the a wireless transmitter using the Serial devices and speeds provided.
//          
//          
// Requires: ECUCAN Library since the it pulls the data for the packets stright
//           from the buffers.
// Notes:   
//         
//----------------------------------------------------------------------------
*/

#ifndef _WirelessDataAcqSerial_h
#define _WirelessDataAcqSerial_h

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

#define SERIAL_PACKET_LENGTH      8   //8 Bytes per sequence

#define SERIAL_HEADER_FIRST_BYTE  10 //Equivalent to backslash n
#define SERIAL_HEADER_SECOND_BYTE 255

#define SERIAL_PACKET_1_ID        0x20
#define SERIAL_PACKET_2_ID        0x21
#define SERIAL_PACKET_3_ID        0x22
#define SERIAL_PACKET_4_ID        0x23
#define SERIAL_PACKET_5_ID        0x10    

class WirelessDataAcqSerial
{
  public:

  WirelessDataAcqSerial(ECUCAN * CAN, HardwareSerial * wirelessSerial, long wirelessSerialSpeed, HardwareSerial * dASerial, long dASerialSpeed);

  void sendData();
  void sendDataWireless();
  void sendDataDA();
  void sendDistanceWireless(uint16_t DistanceKM);
  void sendDistanceDA(uint16_t DistanceKM);

  private:
};

#endif