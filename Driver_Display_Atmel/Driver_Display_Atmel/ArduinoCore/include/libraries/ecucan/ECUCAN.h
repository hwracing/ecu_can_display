/*ECUCAN Header
/----------------------------------------------------------------------------
//(C) COPYRIGHT 2017-2018 HWRacing Formula Student Team
//              ALL RIGHTS RESERVED
// Author : Bruce Thomson
//----------------------------------------------------------------------------
// Purpose: Accepts DTAFast ECU CAN packets and will perform the LSB (Least Significant Byte) 
//          to MSB (Most Significant Byte) conversion. Acts as a wrapper library. Accessor methods
//          are given for each data field of the DTAFAst CAN Stream.
//          See https://www.dtafast.co.uk/download_files/Manuals/S%20Series%20Manual.pdf for details
//          of CAN Stream
// Requires: SeeedStudio Arduino CAN Shield Library (mcp_can.h,mcp_can.cpp, mcp_can_defs.h)
//----------------------------------------------------------------------------
*/

#ifndef _ECUCAN_h
#define _ECUCAN_h

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

//SeeedStudio Library required
#include "mcp_can.h"

//CAN Filter and Masks
#define EXTENDED_ID             1
#define ECU_CAN_PACKET_MASK     0x1FFFFFFF
#define ECU_CAN_FILTER          0x2000 //The filter is the first packet in the range and by modifiying the mask can change the range to accept. 
//When the mask has a b'0 then there is a don't care on the bit of the identifier. When b'1 then the filter must match the packet's identifier for it to accept.

//ECU CAN identifier
#define ECU_CAN_0_IDENT 0x2000
#define ECU_CAN_1_IDENT 0x2001
#define ECU_CAN_2_IDENT 0x2002
#define ECU_CAN_3_IDENT 0x2003
#define ECU_CAN_4_IDENT 0x2004
#define ECU_CAN_5_IDENT 0x2005
#define ECU_CAN_6_IDENT 0x2006
#define ECU_CAN_7_IDENT 0x2007

class ECUCAN
{
  public:

  ECUCAN(byte csPin, SPIClass * spi);

  void setupMaskFilters(uint8_t numberOfPackets);
  uint8_t init();
  static void readPacket();
  
  void enableInterrupt(byte interruptPin);
  unsigned char * getPacketPointer(int packetID);

  uint8_t validPacketReceivedSinceStartup();
  unsigned long timeOfLastPacketReceived();

  short getRPM();
  short getTPS();
  short getWaterTemp();
  short getAirTemp();
  
  short getMAP();
  short getLambda();
  short getSpeed();
  short getOilPress();

  short getFuelPress();
  short getOilTemp();
  short getBatteryVoltage();
  short getFuelConLitresPerHour();

  short getGear();
  short getAdvanceDeg();
  short getInjectionMs();
  short getFuelConLitresPer100Km();

  short getAnalog1Voltage();
  short getAnalog2Voltage();
  short getAnalog3Voltage();
  short getCamAdvance();
  
  short getCamTarget();
  short getCamPWM();
  short getCrankErrors();
  short getCamErrors();

  short getCam2Advance();
  short getCam2Target();
  short getCam2PWM();
  short getExternal5V();

  short InjectorDutyCycle();
  short LambdaPIDTarget();
  short LambdaPIDAdjustment();

  private:

  short convertToMSBBuffer(uint8_t * counter, byte * buffer);
};

#endif