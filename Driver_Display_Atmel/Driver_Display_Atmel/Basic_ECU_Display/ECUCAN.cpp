/*ECUCAN.cpp
/----------------------------------------------------------------------------
//(C) COPYRIGHT 2017-2018 HWRacing Formula Student Team
//              ALL RIGHTS RESERVED
// Author : Bruce Thomson
//----------------------------------------------------------------------------
// Purpose: Accepts DTAFast ECU CAN packets and will perform the LSB (Least Significant Byte) 
//          to MSB (Most Significant Byte) conversion. Acts as a wrapper library. Accessor methods
//          are given for each data field of the DTAFAst CAN Stream.
//          See https://www.dtafast.co.uk/download_files/Manuals/S%20Series%20Manual.pdf for details
//          of CAN Stream
// Requires: SeeedStudio Arduino CAN Shield Library (mcp_can.h,mcp_can.cpp, mcp_can_defs.h)
//----------------------------------------------------------------------------
*/

#include "ECUCAN.h"


//GLOBAL VARIABLES
MCP_CAN CAN;

unsigned char CANReadBuffer[8] = {0, 0, 0, 0, 0, 0, 0, 0}; //Buffer for reading the CAN Packet
byte CANPacketLen = 0;

uint8_t validPacket = 0;
unsigned long lastPacketTime = 0;


//Variables for holding each of the 8 different CAN packets received. 
//Waiting to be decoded by the accessor functions
unsigned char CANPacket0[8]    = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned char CANPacket1[8]    = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned char CANPacket2[8]    = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned char CANPacket3[8]    = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned char CANPacket4[8]    = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned char CANPacket5[8]    = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned char CANPacket6[9]    = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned char CANPacket7[9]    = {0, 0, 0, 0, 0, 0, 0, 0};

//ECUCAN CONSTRUCTOR
//Parameters: csPin - the digital pin used for the CSPin
//Parameters: spi   - the hardware SPI object to be used.
ECUCAN::ECUCAN(byte csPin, SPIClass* spi)
{
  //Declare the new CAN object
  CAN = MCP_CAN(csPin);
  CAN.setSPI(spi);
}

//setupMaskFilters Function setups the CAN packet filters and mask.
//See http://www.cse.dmu.ac.uk/~eg/tele/CanbusIDandMask.html for details
//on how masking and filters work.
//CURRENTLY THIS FUNCTION DOES NOT WORK. DO NOT USE THE MASKING FUNCTIO
//Parameters: numberOfPackets - this is a value between 1 & 8 which 
//                              defines which packets between 0x2000-0x2007
//                              are accepted. For example 4 means only packets with
//                              identifiers 0x2000--0x2003 are accepted.
//                              And 6 means only packets with identifiers between
//                              0x2000-2005 are accepted.
void ECUCAN::setupMaskFilters(uint8_t numberOfPackets)
{
  unsigned long ECUMask = ECU_CAN_PACKET_MASK;
  
  CAN.init_Mask(0, EXTENDED_ID, 0x1FFFFFFF);
  CAN.init_Mask(1, EXTENDED_ID, 0x1FFFFFFE);
  CAN.init_Filt(0, EXTENDED_ID, 0x2003);
  CAN.init_Filt(1, EXTENDED_ID, 0x1999);
  CAN.init_Filt(2, EXTENDED_ID, 0x2000);
  CAN.init_Filt(3, EXTENDED_ID, 0x2001);
  CAN.init_Filt(4, EXTENDED_ID, 0x2002);
  
  /*
  switch (numberOfPackets)
  {
    case (6):
      CAN.init_Filt(5, EXTENDED_ID, ECU_CAN_5_IDENT);
    case (5):
      CAN.init_Filt(4, EXTENDED_ID, ECU_CAN_4_IDENT);
    case (4):
      CAN.init_Filt(3, EXTENDED_ID, ECU_CAN_3_IDENT);
    case (3):
      CAN.init_Filt(2, EXTENDED_ID, ECU_CAN_2_IDENT);
    case (2):
      CAN.init_Filt(1, EXTENDED_ID, ECU_CAN_1_IDENT);
    case (1):
      CAN.init_Filt(0, EXTENDED_ID, ECU_CAN_0_IDENT);
      break;
    default:
      break;
  }
  */
  
}


//init Function begins the CAN communications at 1Mb/s. The function will attempt to connect
//to the MCP2515 10 times before returning 1. Function returns 0 if connection was a success.
uint8_t ECUCAN::init()
{
  //Attempts to connect to the MCP2515 10 times before returning an error.
  uint8_t attempts = 0;
  if(CAN_OK == CAN.begin(CAN_1000KBPS, MCP_10MHz) != 0) // 10MHz for Click Board
    return 0;
  else
    return 1;
}

//readPacket function reads all CAN frames in the message buffers and stores 
//the latest of the 8 CAN frames in the CANPacketX variables for awaiting to be
//decoded.
static void readPacket()
{
    unsigned long receivedPacketID;
    while (CAN_MSGAVAIL == CAN.checkReceive())
    {
      CAN.readMsgBufID(&receivedPacketID,&CANPacketLen,CANReadBuffer);
      switch(receivedPacketID)
      {
        case ECU_CAN_0_IDENT:
          memcpy(CANPacket0, CANReadBuffer, 8);
          break;
        case ECU_CAN_1_IDENT:
          memcpy(CANPacket1, CANReadBuffer, 8);
          break;
        case ECU_CAN_2_IDENT:
          memcpy(CANPacket2, CANReadBuffer, 8);
          break;
        case ECU_CAN_3_IDENT:
          memcpy(CANPacket3, CANReadBuffer, 8);
          validPacket = 1;
          break;
        case ECU_CAN_4_IDENT:
          memcpy(CANPacket4, CANReadBuffer, 8);
          break;
        case ECU_CAN_5_IDENT:
          memcpy(CANPacket5, CANReadBuffer, 8);
          break;
        case ECU_CAN_6_IDENT:
          memcpy(CANPacket6, CANReadBuffer, 8);
          break;
        case ECU_CAN_7_IDENT:
          memcpy(CANPacket7, CANReadBuffer, 8);
          break;
        default:
          break;
      }
      
      //Store the time that the packet was read
      lastPacketTime = millis();
    }
}

//enableInterrupts function enables interupts from the MCP2515's interrupt pin on the falling edge for the provided
//digital pin. However, will only work on pins 2 and 3 on Uno & Mini and Pins 2,3,18,19,20,21 for Mega.
//Uses normal Arduino Interrupts and will be disabled by nointerrupts();
//Use of interrupts means no calls to readPacket from the main program are required.
void ECUCAN::enableInterrupt(byte interruptPin)
{
  attachInterrupt(digitalPinToInterrupt(interruptPin), readPacket, FALLING);
}

//convertToMSBBuffer function combines two bytes from the CAN packet provided
//and switches the data from LSB to MSB. Least Significant to Most Significant Byter
//Parameters: counter - this is a pointer to a value which indicates the index of the first byte of the
//                      value. 
//            buffer - this is a pointer to the byte buffer which contains the CAN packet values
short ECUCAN::convertToMSBBuffer(uint8_t * counter, byte * buffer)
{
   unsigned short value;
   unsigned short tempValue;

   //The first value is the least significant byte and therefore doesn't need to be shifted.
   value = buffer[*counter];
   *counter = *counter + 1;

   //The second value is the most significant byte and needs to be left shifted 8 bits before
   //being bitwise OR to get the correct value.
   tempValue = buffer[*counter];
   tempValue = tempValue << 8;
   value = tempValue | value;
   *counter = *counter + 1;
   return (short) value;
}
//getPacketPointer function returns a pointer to the 8 byte CAN packet before any LSB - MSB
//decoding happens. Used for sending data wirelessly and to DA system.
unsigned char * ECUCAN::getPacketPointer(int packetID)
{
  switch (packetID)
  {
    case ECU_CAN_0_IDENT:
      return CANPacket0;
    case ECU_CAN_1_IDENT:
      return CANPacket1;
    case ECU_CAN_2_IDENT:
      return CANPacket2;
    case ECU_CAN_3_IDENT:
      return CANPacket3;
    case ECU_CAN_4_IDENT:
      return CANPacket4;
    case ECU_CAN_5_IDENT:
      return CANPacket5;
    case ECU_CAN_6_IDENT:
      return CANPacket6;
    case ECU_CAN_7_IDENT:
      return CANPacket7;
    default:
      return NULL;
  }
}

//validPacketReceivedSinceStartup function which checks whether there has been a valid packet received since
//start up. Returns 1 if valid otherwise 0.
uint8_t ECUCAN::validPacketReceivedSinceStartup()
{
  return validPacket;
}

//timeOfLastPacketReceived
unsigned long ECUCAN::timeOfLastPacketReceived()
{
  return lastPacketTime;
}
//Accessor methods for getting the values from the data stream.
//See See https://www.dtafast.co.uk/download_files/Manuals/S%20Series%20Manual.pdf for details of CAN Stream values meaning

short ECUCAN::getRPM()
{
  uint8_t counter = 0;
  return convertToMSBBuffer(&counter,CANPacket0);
}

short ECUCAN::getTPS()
{
  uint8_t counter = 2;
  return convertToMSBBuffer(&counter,CANPacket0);
}

short ECUCAN::getWaterTemp()
{
  uint8_t counter = 4;
  return convertToMSBBuffer(&counter,CANPacket0);
}

short ECUCAN::getAirTemp()
{
  uint8_t counter = 6;
  return convertToMSBBuffer(&counter,CANPacket0);
}

short ECUCAN::getMAP()
{
  uint8_t counter = 0;
  return convertToMSBBuffer(&counter,CANPacket1);
}

short ECUCAN::getLambda()
{
  uint8_t counter = 2;
  return convertToMSBBuffer(&counter,CANPacket1);
}

short ECUCAN::getSpeed()
{
  uint8_t counter = 4;
  return convertToMSBBuffer(&counter,CANPacket1);
}

short ECUCAN::getOilPress()
{
  uint8_t counter = 6;
  return convertToMSBBuffer(&counter,CANPacket1);
}

short ECUCAN::getFuelPress()
{
  uint8_t counter = 0;
  return convertToMSBBuffer(&counter,CANPacket2);
}

short ECUCAN::getOilTemp()
{
  uint8_t counter = 2;
  return convertToMSBBuffer(&counter,CANPacket2);
}

short ECUCAN::getBatteryVoltage()
{
  uint8_t counter = 4;
  return convertToMSBBuffer(&counter,CANPacket2);
}

short ECUCAN::getFuelConLitresPerHour()
{
  uint8_t counter = 6;
  return convertToMSBBuffer(&counter,CANPacket2);
}

short ECUCAN::getGear()
{
  uint8_t counter = 0;
  return convertToMSBBuffer(&counter,CANPacket3);
}

short ECUCAN::getAdvanceDeg()
{
  uint8_t counter = 2;
  return convertToMSBBuffer(&counter,CANPacket3);
}

short ECUCAN::getInjectionMs()
{
  uint8_t counter = 4;
  return convertToMSBBuffer(&counter,CANPacket3);
}

short ECUCAN::getFuelConLitresPer100Km()
{
  uint8_t counter = 6;
  return convertToMSBBuffer(&counter,CANPacket3);
}

short ECUCAN::getAnalog1Voltage()
{
  uint8_t counter = 0;
  return convertToMSBBuffer(&counter,CANPacket4);
}

short ECUCAN::getAnalog2Voltage()
{
  uint8_t counter = 2;
  return convertToMSBBuffer(&counter,CANPacket4);
}

short ECUCAN::getAnalog3Voltage()
{
  uint8_t counter = 4;
  return convertToMSBBuffer(&counter,CANPacket4);
}

short ECUCAN::getCamAdvance()
{
  uint8_t counter = 6;
  return convertToMSBBuffer(&counter,CANPacket4);
}

short ECUCAN::getCamTarget()
{
  uint8_t counter = 0;
  return convertToMSBBuffer(&counter,CANPacket5);
}

short ECUCAN::getCamPWM()
{
  uint8_t counter = 2;
  return convertToMSBBuffer(&counter,CANPacket5);
}

short ECUCAN::getCrankErrors()
{
  uint8_t counter = 4;
  return convertToMSBBuffer(&counter,CANPacket5);
}

short ECUCAN::getCamErrors()
{
  uint8_t counter = 6;
  return convertToMSBBuffer(&counter,CANPacket5);
}

short ECUCAN::getCam2Advance()
{
  uint8_t counter = 0;
  return convertToMSBBuffer(&counter,CANPacket6);
}

short ECUCAN::getCam2Target()
{
  uint8_t counter = 2;
  return convertToMSBBuffer(&counter,CANPacket6);
}

short ECUCAN::getCam2PWM()
{
  uint8_t counter = 4;
  return convertToMSBBuffer(&counter,CANPacket6);
}

short ECUCAN::getExternal5V()
{
  uint8_t counter = 6;
  return convertToMSBBuffer(&counter,CANPacket6);
}

short ECUCAN::InjectorDutyCycle()
{
  uint8_t counter = 0;
  return convertToMSBBuffer(&counter,CANPacket7);
}

short ECUCAN::LambdaPIDTarget()
{
  uint8_t counter = 2;
  return convertToMSBBuffer(&counter,CANPacket7);
}

short ECUCAN::LambdaPIDAdjustment()
{
  uint8_t counter = 4;
  return convertToMSBBuffer(&counter,CANPacket7);
}
