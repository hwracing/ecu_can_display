
//Library for calculating the distance using the Timer 5 Counter
//Uses Pin 47
//
#include "DistanceTravelled.h" 
#include "Arduino.h"

int overflows = 0;

DistanceTravelled::DistanceTravelled(uint8_t dummy_value)
{

}

void DistanceTravelled::init()
{
  setupTimer5();
  loadTimer5(readEEPROMTicks());
  startTimer5();
}

void DistanceTravelled::reset()
{
  stopTimer5();
  setupTimer5();
  loadTimer5(0);
  startTimer5();
  resetEEPROM();
}

void DistanceTravelled::setupTimer5()
{
  TCCR5A = 0; //          Timer5 Control Register A 
  TCCR5B = 0;

  TIMSK5 = bit (TOIE5);  

  GTCCR = bit (PSRASY);
}

void DistanceTravelled::startTimer5()
{
  TCCR5B =  bit (CS50) | bit (CS51) | bit (CS52);
}

void DistanceTravelled::stopTimer5()
{
  TCCR5A = 0;
  TCCR5B = 0;
  TIMSK5 = 0; 
}

uint16_t DistanceTravelled::readTimer5()
{
  noInterrupts();
  uint16_t value = TCNT5;
  interrupts();
  return value;
}

void DistanceTravelled::loadTimer5(uint16_t ticks)
{
  noInterrupts();
  TCNT5 = ticks;
  interrupts();
}

uint16_t DistanceTravelled::readEEPROMTicks()
{
  uint8_t firstByte = EEPROM.read(DISTANCE_TICKS_ADDRESS);
  uint8_t secondByte = EEPROM.read(DISTANCE_TICKS_ADDRESS+1);
  uint16_t data = firstByte << 8;
  data = data | secondByte;
  return data;
}

uint16_t DistanceTravelled::readEEPROMOverflows()
{
  uint8_t firstByte = EEPROM.read(DISTANCE_OVERFLOW_ADDRESS);
  uint8_t secondByte = EEPROM.read(DISTANCE_OVERFLOW_ADDRESS+1);
  uint16_t data = firstByte << 8;
  data = data | secondByte;
  data--;
  //Return 1 less overflow since there is always an overflow on startup.
  return data;
}

void DistanceTravelled::writeDistance()
{
  writeTicks(readTimer5());
}

void DistanceTravelled::writeTicks(uint16_t ticks)
{
  uint8_t firstByte = ticks >> 8;
  uint8_t secondByte = ticks;
  EEPROM.update(DISTANCE_TICKS_ADDRESS,firstByte);
  EEPROM.update(DISTANCE_TICKS_ADDRESS+1,secondByte); 
}

void DistanceTravelled::writeOverflows(uint16_t overflows)
{
  uint8_t firstByte = overflows >> 8;
  uint8_t secondByte = overflows;
  EEPROM.update(DISTANCE_OVERFLOW_ADDRESS,firstByte);
  EEPROM.update(DISTANCE_OVERFLOW_ADDRESS+1,secondByte); 
}

uint32_t DistanceTravelled::getDistanceCM()
{
  uint32_t numberOfOverflows = readEEPROMOverflows();
  uint32_t distanceOverflows = numberOfOverflows * OVERFLOW_DISTANCE;

  uint32_t currentTicks     = readTimer5(); //TCNT5 contaisn the current ticks
  uint32_t distanceTicks     = currentTicks * DISTANCE_PER_TICK;

  return distanceTicks + distanceOverflows;
}


double DistanceTravelled::getDistanceKM()
{
  double distanceInt = getDistanceCM() / 100000.0;
  Serial.print("Distance");
  Serial.println(distanceInt, DEC);
  return (double) getDistanceCM() / 100000.0;
}

uint16_t DistanceTravelled::getDistanceKMInteger()
{
  uint32_t distanceInt = getDistanceCM() / 100000;
  Serial.print("Distance");
  Serial.println(distanceInt, DEC);
  return distanceInt;
}


void DistanceTravelled::resetEEPROM()
{
  uint8_t value = 0;
  EEPROM.write(DISTANCE_OVERFLOW_ADDRESS, value);
  EEPROM.write(DISTANCE_OVERFLOW_ADDRESS+1, value);
  EEPROM.write(DISTANCE_TICKS_ADDRESS, value);
  EEPROM.write(DISTANCE_TICKS_ADDRESS+1, value);
  
  #ifdef DEBUG
    Serial.println("EPPROM RESET");
    Serial.println(EEPROM.read(DISTANCE_TICKS_ADDRESS), BIN);
    Serial.println(EEPROM.read(DISTANCE_TICKS_ADDRESS+1), BIN);
  #endif
}


//Interupt for when the timer reaches capacity
ISR(TIMER5_OVF_vect)
{
  //Serial printout to enable when an overflow is triggered
  #ifdef DEBUG
    Serial.println("OF");
  #endif
  
  uint8_t firstByte = EEPROM.read(DISTANCE_OVERFLOW_ADDRESS);
  uint8_t secondByte = EEPROM.read(DISTANCE_OVERFLOW_ADDRESS+1);
  uint16_t numberOfOverflows = firstByte<<8;
  numberOfOverflows = numberOfOverflows | secondByte;

  numberOfOverflows++;
  firstByte = numberOfOverflows>>8;
  secondByte = numberOfOverflows & 0xFF;
  EEPROM.update(DISTANCE_OVERFLOW_ADDRESS, firstByte);
  EEPROM.update(DISTANCE_OVERFLOW_ADDRESS+1, secondByte);
}