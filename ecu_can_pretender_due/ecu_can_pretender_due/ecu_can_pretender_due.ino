/*
Program to pretend to be a DTAFast ECU sending out CAN and then receiving it.

Copyright HWRacing Formula Student Team 2017

*/
//LIBRARIES TO INCLUDE

#define DEBUG_EN 1

#include <mcp_can.h>
#include <SPI.h>

//RANDOM CONSTANTS
#define MAX_RPM         12000
#define MAX_WATER_TEMP  120
#define MAX_AIR_TEMP    70

#define MAX_MAP         300
#define MAX_LAMBDA      1550 // Lambda*1000
#define MAX_SPEED       1300 //(Around 80MPH = 130KPH. KPH Value * 10)
#define MAX_OILPRESS    500

#define MAX_FUELPRESS   1000
#define MAX_OILTEMP     120
#define MIN_BATVOLT     80  //Battery Voltage * 10
#define MAX_BATVOLT     140 //Battery Voltage * 10
#define MAX_FUELCONLH   60  //Fuel Consumption L per Hour *10

#define MAX_GEAR        6
#define MAX_ADVANCEDEG  2   //Advance Degree * 10
#define MAX_INJECTION   2000//Injection Speed ms *100
#define MAX_FUELCONLKM  5

//CONSTANTS
#define SPI_CAN_CS_PIN 10 //SPI Chip Select Pin on Arduino Uno

#define CAN_1_IDENT 0x2000
#define CAN_2_IDENT 0x2001
#define CAN_3_IDENT 0x2002
#define CAN_4_IDENT 0x2003
#define CAN_5_IDENT 0x2004
#define CAN_6_IDENT 0x2005
#define CAN_7_IDENT 0x2006
#define CAN_8_IDENT 0x2007

//GLOBAL VARIABLES & OBJECTS
MCP_CAN CAN(SPI_CAN_CS_PIN);

//Packet 1 Global Variables
short RPM;
short TPS;
short WaterTemp;
short AirTemp;

//Packet 2 Global Variables
short MAP;
short Lambda;
short SpeedKph;
short OilPres;

//Packet 3 Global Variables
short  FuelPres;
short  OilTemp;
short  BatVolt;
short  FuelConLH;

//Packet 4 Global Variables
short Gear;
short AdvanceDeg;
short Injection;
short FuelConLKM;

//Buffer
byte sendBuffer[8] = {0, 0, 0, 0, 0, 0, 0, 0};
short sendBufferCounter;


void setup()
{
  SerialUSB.begin(115200); //PC Serial
  SerialUSB.println("Hello World");
  pinMode(SPI_CAN_CS_PIN, OUTPUT);
  CAN.setSPI(&SPI);
  SPI.beginTransaction(SPISettings(10000000, MSBFIRST, SPI_MODE0));

  while (CAN.begin(CAN_1000KBPS,MCP_10MHz) != CAN_OK)
  {
    SerialUSB.println("CAN BUS Initialisation Failed");
    delay(1);
  }

  SerialUSB.println("CAN BUS INIT PASSED!");

  /*
  //Setup Timer 1 to trigger the generation and sending of data every 10Hz
  //Nicked from http://www.instructables.com/id/Arduino-Timer-Interrupts/
  cli(); //Disable Interrupts
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1  = 0;

  OCR1A = 768; //Gives 20Hz Interupt
  //OCR1A = 15624;
  //Turn on CTC (Compare Mode)
  TCCR1A |= (1 << WGM12);
  TCCR1B |= (1 << CS12) | (1 <CS10); 
  TIMSK1 |= (1 << OCIE1A);
  
  sei(); //Allow interupts
  */
}
/*
//Timer 1 Interupt Service Routine. Place calls to methods which generate the CAN Packets here. Will trigger 10Hz
ISR(TIMER1_COMPA_vect)
{
  digitalWrite(13,HIGH);
  generateFirstPacket();
  generateSecondPacket();
  generateThirdPacket();
  generateFourthPacket();
  digitalWrite(13,LOW);
  Serial.println("Sent Packets");
}
*/
void loop()
{
  digitalWrite(10,LOW);
  generateFirstPacket();
  generateSecondPacket();
  generateThirdPacket();
  generateFourthPacket();
  digitalWrite(10,HIGH);
  SerialUSB.println("Sent Packets");
  delay(200);
  
}

bool generateFirstPacket()
{
  RPM       = random(MAX_RPM);
  TPS       = random(100);
  WaterTemp = random(MAX_WATER_TEMP);
  AirTemp   = random(MAX_AIR_TEMP);
  
  sendBufferCounter = 0;
  convertToLSBBuffer(RPM, &sendBufferCounter, sendBuffer);       //RPM
  convertToLSBBuffer(TPS, &sendBufferCounter, sendBuffer);       //TPS
  convertToLSBBuffer(WaterTemp,&sendBufferCounter, sendBuffer); //Water Temp
  convertToLSBBuffer(AirTemp, &sendBufferCounter, sendBuffer);  //Air Temp

  CAN.sendMsgBuf(CAN_1_IDENT, 1, 8, sendBuffer);
  return 1;
}

bool generateSecondPacket()
{
  MAP      = random(MAX_MAP);
  Lambda   = random(MAX_LAMBDA);
  SpeedKph = random(MAX_SPEED+1);
  OilPres  = random(MAX_OILPRESS);

  sendBufferCounter = 0;

  convertToLSBBuffer(MAP, &sendBufferCounter, sendBuffer);       //RPM
  convertToLSBBuffer(Lambda, &sendBufferCounter, sendBuffer);       //TPS
  convertToLSBBuffer(SpeedKph,&sendBufferCounter, sendBuffer); //Water Temp
  convertToLSBBuffer(OilPres, &sendBufferCounter, sendBuffer);  //Air Temp

  CAN.sendMsgBuf(CAN_2_IDENT, 1, 8, sendBuffer);
  return 1;
}

bool generateThirdPacket()
{
  FuelPres  = random(MAX_FUELPRESS);
  OilTemp   = random(MAX_OILTEMP);
  BatVolt   = random(MIN_BATVOLT, MAX_BATVOLT);
  FuelConLH = random(MAX_FUELCONLH);

  sendBufferCounter = 0;

  convertToLSBBuffer(FuelPres, &sendBufferCounter, sendBuffer);       //RPM
  convertToLSBBuffer(OilTemp, &sendBufferCounter, sendBuffer);       //TPS
  convertToLSBBuffer(BatVolt,&sendBufferCounter, sendBuffer); //Water Temp
  convertToLSBBuffer(FuelConLH, &sendBufferCounter, sendBuffer);  //Air Temp

  CAN.sendMsgBuf(CAN_3_IDENT, 1, 8, sendBuffer);
  return 1;
}

bool generateFourthPacket()
{
  Gear       = random(MAX_GEAR+1);
  AdvanceDeg = random(MAX_ADVANCEDEG);
  Injection  = random(MAX_INJECTION);
  FuelConLKM = random(MAX_FUELCONLKM);

  sendBufferCounter = 0;

  convertToLSBBuffer(Gear, &sendBufferCounter, sendBuffer);       //RPM
  convertToLSBBuffer(AdvanceDeg, &sendBufferCounter, sendBuffer);       //TPS
  convertToLSBBuffer(Injection,&sendBufferCounter, sendBuffer); //Water Temp
  convertToLSBBuffer(FuelConLKM, &sendBufferCounter, sendBuffer);  //Air Temp

  CAN.sendMsgBuf(CAN_4_IDENT, 1, 8, sendBuffer);
  return 1;
}

void convertToLSBBuffer(short valueToShift, short * counter,byte * buffer)
{
  short MSB = valueToShift;
  buffer[*counter] = (byte) MSB; 
  *counter = *counter + 1;
  MSB = MSB >> 8; 
  buffer[*counter] = (byte) MSB;
  *counter = *counter + 1;
}

/*
  Method: 
*/
short convertToLSB(short valueToShift)
{
  short MSB = valueToShift;
  MSB = MSB>>8;
  short LSB = valueToShift;
  LSB = LSB<<8;
  return MSB | LSB;
}
