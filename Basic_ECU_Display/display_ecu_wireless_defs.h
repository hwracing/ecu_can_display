/*display_ecu_wireless_defs.h
/----------------------------------------------------------------------------
//(C) COPYRIGHT 2018 HWRacing Formula Student Team
//              ALL RIGHTS RESERVED
// Author : Bruce Thomson
//----------------------------------------------------------------------------
// Purpose: Contains parameters used by all wrapper libraries
// Requires: Modified Nextion Display Arduino Library
//----------------------------------------------------------------------------
*/
#include <Arduino.h>

#define DEBUG             0
//#define DEBUG_C             1

#define CAN_CS_PIN        10
#define CAN_INTERRUPT_PIN 2
#define CAN_NO_FILTERS    4

#define SCREEN_DETECTION_PIN 5

#define WIRELESS_SPEED    115200

#define DA_SPEED          115200

#define SCREEN_UPDATE_INTERVAL     40 //In Milliseconds
#define WIRELESS_SERIAL_INTERVAL   100 //In Milliseconds
#define DA_SERIAL_INTERVAL         100    //In Milliseconds
#define DISTANCE_UPDATE_INTERVAL   10000 //In Milliseconds

#define CAN_GEAR_WAIT_DELAY        1000 // In Milliseconds
#define CAN_INFO_WARN_DELAY        1000 // In Milliseconds
