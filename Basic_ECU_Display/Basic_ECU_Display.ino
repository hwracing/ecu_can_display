

#include "display_ecu_wireless_defs.h"
#include "ECUCAN.h"
#include "WirelessDataAcqSerial.h"
#include "Display.h"
#include "Nextion.h"
#include "SoftReset.h"

//ECUCAN DTA Fast Wrapper Library
ECUCAN Can(CAN_CS_PIN,&SPI);

//WirelessDataAcqSerial Library. Sends information to the wireless modules
//and 
WirelessDataAcqSerial WirelessData(&Can, NULL, WIRELESS_SPEED, NULL, DA_SPEED);


//Display Wrapper Library. Takes in the CAN Object so it
//to access the fields of the CAN packet.
Display Display(&Can);


//Previous Millis Time. Used for multitasking
unsigned long previousDisplayTime = 0;
unsigned long previousDistanceTime = 0;
unsigned long previousWirelessTime = 0;
unsigned long previousDATime      = 0;
unsigned long currentTime         = 0;

//Boolean to hold the value of the screen
bool prevScreenConnected = false;


// initCAN function calls the initalisation code
// on the MCP2515 and setup the interrupt and filters.
uint8_t initCAN()
{
  Display.CANStarted();
  if (Can.init())
  {
    #ifdef DEBUG
      Serial.println("CAN DISABLED");
    #endif

    Display.setCANStatus(0);
    return 0;
  }
  
  #ifdef DEBUG
    Serial.println("CAN ENABLED");
  #endif
  
  Can.setupMaskFilters(1);
  Display.setCANStatus(1);

  Can.enableInterrupt(CAN_INTERRUPT_PIN);

  Display.CANFinished();
}

void CANLoop()
{
  //Wait for the First CAN packet to come in
  long currentTime = millis();
  long lastTime = currentTime;
  uint8_t CAN_ConnectionAttempts = 0;

  while (CAN_ConnectionAttempts < 10)
  {
    currentTime = millis();
    if (currentTime >  lastTime + 100)
    {
      initCAN();
      lastTime = currentTime;
      CAN_ConnectionAttempts++;
    }
    
    if (Can.validPacketReceivedSinceStartup() == 1)
    {
      Serial.print("CAN ATTEMPTS");
      Serial.println(CAN_ConnectionAttempts, DEC);
      break;
    }
  }

  /*uint8_t numberOfAttemptsToConnectECU = EEPROM.read(1);

  if (Can.validPacketReceivedSinceStartup() == 0 && numberOfAttemptsToConnectECU < 4)
  {
    numberOfAttemptsToConnectECU++;
    EEPROM.write(1,numberOfAttemptsToConnectECU);
    soft_restart();
  }
  else
  {
    EEPROM.write(1,0);
  }
  */
  return 1;
}

bool isScreenConnected()
{
  return digitalRead(SCREEN_DETECTION_PIN);
}
//Arduino Setup function 
void setup()
{
  //Setup the ScreenConnected detection pin
  pinMode(SCREEN_DETECTION_PIN ,INPUT);

  #ifdef DEBUG
    Serial.begin(115200);
    Serial.println("Serial Debugging Enabled at 115200 Baud");
  #endif

  while (!isScreenConnected())
  {
    Serial.println("Screen is not connected");
    delay(100); //Check again in 100ms
  }

  Display.startup();
  //Setup the Callbacks for the buttons on Screen
  Display.attachCallBacks();

  //Initialises CAN and tells the screen if CAN is working.
  CANLoop();
  
  //setupComplete moves the display from the INIT_PAGE to 
  //the DASH_PAGE if CAN is successful. However, will display
  //error messages.

  Display.setupComplete();
  Display.attachChangeInterrupt();

  prevScreenConnected = isScreenConnected();
}

//Arduino Loop function 
void loop()
{
  if (isScreenConnected() && prevScreenConnected)
  {
    //Can.readPacket();
    //get the current time so we can enter the loop;
    currentTime = millis();

    //Handle Any Display Callbacks
    Display.runDisplayLoop();

    //Update The Display's Values
    if (currentTime - previousDisplayTime > SCREEN_UPDATE_INTERVAL)
    { 
      #ifdef DEBUG
        Serial.print("Update Display Triggered at:");
        Serial.println("");
        Serial.print("CAN GEAR");
        Serial.println(Can.getGear(),DEC);
    //  Serial.println(currentTime,DEC);
      #endif
      //Update when the last CAN packet received is within an acceptable level. Otherwise display warning to Driver.
      if (currentTime > Can.timeOfLastPacketReceived() + CAN_INFO_WARN_DELAY)
      {
        Display.infoWarning();
      }
      else
      {
        Display.updateValues();
      }
      previousDisplayTime = currentTime;
      currentTime = millis();
    }
  }
/*
    //Update Distance. Store the values and send distance data to DA and Wireless
    if (currentTime - previousDistanceTime > DISTANCE_UPDATE_INTERVAL)
    {
      Serial.print("Update Distance");
      uint16_t distanceTravelled = 54;
      Serial.println(distanceTravelled,DEC);
      Display.updateDistance(distanceTravelled);
      WirelessData.sendDistanceWireless(distanceTravelled);
      WirelessData.sendDistanceDA(distanceTravelled);
    
      previousDistanceTime = currentTime;
      currentTime = millis();
    }
  }
  else if (!isScreenConnected())
  {
    #ifdef DEBUG
      Serial.println("Screen not connected");
    #endif
    prevScreenConnected = false;
  }
  else if ((isScreenConnected()) && (!prevScreenConnected)) 
  {
    delay(1000);
    prevScreenConnected = true;
    Display.sendCurrentDisplay();
  }

    bool canPacketValid = currentTime <= Can.timeOfLastPacketReceived() + CAN_INFO_WARN_DELAY;

    if ((currentTime - previousWirelessTime > WIRELESS_SERIAL_INTERVAL) && (canPacketValid))
    {
      #ifdef DEBUG
        Serial.print("Wireless Serial Triggered at:");
      // Serial.println(currentTime,DEC);
      #endif

      WirelessData.sendDataWireless();
      previousWirelessTime = currentTime;
      currentTime = millis();
    }

    if ((currentTime - previousDATime > DA_SERIAL_INTERVAL) && (canPacketValid))
    {
      #ifdef DEBUG
        Serial.print("Data Serial Triggered at:");
        //Serial.println(currentTime,DEC);
      #endif

      WirelessData.sendDataDA();
      previousDATime = currentTime;
      currentTime = millis();
    }
  */
}
