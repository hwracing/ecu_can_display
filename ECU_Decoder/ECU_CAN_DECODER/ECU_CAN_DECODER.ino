/*
  ECU_CAN_DECODER. This sketch decodes the the CAN input from
*/

//LIBRARIES
#include <SPI.h>
#include "mcp_can.h"

//DEFINES
#define CAN_CS_PIN  53
#define CAN_INT_PIN 2 //2 or 3 on an Uno

//CAN IDENT DEFINES
#define ECU_CAN_1_IDENT 0x2000
#define ECU_CAN_2_IDENT 0x2001
#define ECU_CAN_3_IDENT 0x2002
#define ECU_CAN_4_IDENT 0x2003
#define ECU_CAN_5_IDENT 0x2004
#define ECU_CAN_6_IDENT 0x2005
#define ECU_CAN_7_IDENT 0x2006
#define ECU_CAN_8_IDENT 0x2007


//CAN FILTER DEFINES.
//See http://www.cse.dmu.ac.uk/~eg/tele/CanbusIDandMask.html for details on
//how masking works
#define ECU_CAN_MASK    0x1FFFFFFF //This Mask and Filter will accept packets with identifiers 0x2000 - 0x2003 
#define ECU_CAN_FILTER_0  0x2000     //The filter is the first packet in the range and by modifiying the mask can change the range to accept. 
#define ECU_CAN_FILTER_1  0x2001
#define ECU_CAN_FILTER_2  0x2002
#define ECU_CAN_FILTER_3  0x2003
//When the mask has a b'0 then there is a don't care on the bit of the identifier. When b'1 then the filter must match the packet's identifier for it to accept.

//GLOBAL VARIABLES

MCP_CAN CAN(CAN_CS_PIN); //Initiates a CAN object

unsigned char CANReadBuffer[8] = {0, 0, 0, 0, 0, 0, 0, 0}; //Buffer for reading the CAN Packet
byte CANPacketLen = 0;

volatile uint8_t CANInterruptTriggered;

//GLOBAL
short RPM;
short TPS;
short WaterTemp;
short AirTemp;
short MAP;
short Lambda;
short Speed;
short OilPress;
short FuelPress;
short OilTemp;
short BatVoltage;
short FuelConLH;
short Gear;

void setup()
{
  Serial.begin(500000);
  Serial.println("Hello");
  while (CAN_OK != CAN.begin(CAN_1000KBPS, MCP_10MHz))
  {
    Serial.println("CAN BUS INIT FAILED");
    delay(1000);
  }

  Serial.println("CAN BUS INITIALISED");

  //Initialise both masks of the MCP2515 to the required mask.
  //CAN.init_Mask(0, 1, ECU_CAN_MASK); //Parameters are CAN.init_Mask(MCP_MASK_REG, IF_EXTENDED_IDENT, MASK)
  //CAN.init_Mask(1, 1, ECU_CAN_MASK);

  //Intitalise the first filter to accept the ECU can Filter. There are 6 filters on a MCP2515
 // CAN.init_Filt(0, 1, ECU_CAN_FILTER_0);
  //CAN.init_Filt(1, 1, ECU_CAN_FILTER_1);
 // CAN.init_Filt(3, 1, ECU_CAN_FILTER_3);
 // CAN.init_Filt(2, 1, ECU_CAN_FILTER_2);

  //Attach the interrupt on a packet received on the MCP2515. The MCP2515 operates on a falling edge interrupt.
  attachInterrupt(digitalPinToInterrupt(CAN_INT_PIN), CAN_ISR, FALLING);
  CANInterruptTriggered = 0;
}

void CAN_ISR()
{
  CANInterruptTriggered = 1;
  readPacket();
}

void decodeCANPacket(unsigned char * buf, uint32_t * packetID)
{
 uint8_t bufferCounter = 0;
  
  switch (*packetID)
  {
    //
    //case ECU_CAN_1_IDENT :
      /*RPM = convertToMSBBuffer(&bufferCounter, buf);
      TPS = convertToMSBBuffer(&bufferCounter, buf);
      WaterTemp = convertToMSBBuffer(&bufferCounter, buf);
      AirTemp = convertToMSBBuffer(&bufferCounter, buf);
      Serial.println("0x2000 Packet");
      Serial.println("RPM\tTPS\tWaterTemp\tAirTemp");
      Serial.print(RPM,DEC);
      Serial.print("\t");
      Serial.print(TPS,DEC);
      Serial.print("\t");
      Serial.print(WaterTemp, DEC);
      Serial.print("\t");
      Serial.println(AirTemp, DEC);
      */
      //Serial.print("Another Packet Received");
      //Serial.println(*packetID, HEX);
     // break;

    //0x2001 Packet contains: Manifold Air Pressure, Lambda, Speed in KPH, Oil Pressure 
    //case ECU_CAN_2_IDENT :
     // Serial.print("Another Packet Received");
     // Serial.println(*packetID, HEX);

     // break;

    //0x2002 Packet contains: Fuel Pressure, Oil Temperature, Battery Voltage, Fuel Consumption
    //case ECU_CAN_3_IDENT : 
     // FuelPress = convertToMSBBuffer(&bufferCounter, buf);
     // OilTemp = convertToMSBBuffer(&bufferCounter,buf);
     // BatVoltage = convertToMSBBuffer(&bufferCounter, buf);
     // FuelConLH = convertToMSBBuffer(&bufferCounter, buf);

     // Serial.println("0x2002 Packet");
     // Serial.println("Fuel Press\tOil Temp\tBattery Voltage\tFuel Con\t");
     // Serial.print(FuelPress, DEC);
     // Serial.print("\t");
     // Serial.print(OilTemp, DEC);
     // Serial.print("\t");
     // Serial.print(BatVoltage, DEC);
     // Serial.print("\t");
     // Serial.println(FuelConLH, DEC);
     //Serial.print("Another Packet Received");
     //Serial.println(*packetID, HEX);

      //break;
    //case ECU_CAN_4_IDENT :
     // Gear = convertToMSBBuffer(&bufferCounter, buf);
      //Serial.println("0x2003 Packet");
      //Serial.print("Gear: ");
      //Serial.println(Gear, DEC);
      
      //break;
    default :
      Serial.println(*packetID, HEX);
  }
}

short convertToMSBBuffer(uint8_t * counter, byte * buffer)
{
   unsigned short value;
   unsigned short tempValue;

   value = buffer[*counter];
   *counter = *counter + 1;
   tempValue = buffer[*counter];
   tempValue = tempValue << 8;
   value = tempValue | value;
   *counter = *counter + 1;
   return (short) value;
}

void readPacket()
{
    uint32_t receivedPacketID;

  if (CANInterruptTriggered)
  {
    while (CAN_MSGAVAIL == CAN.checkReceive())
    {
      CAN.readMsgBuf(&CANPacketLen, CANReadBuffer);
      receivedPacketID = CAN.getCanId();

      decodeCANPacket(CANReadBuffer, &receivedPacketID);

      CANInterruptTriggered = 0;
    }
  }
}

void loop()
{
}
