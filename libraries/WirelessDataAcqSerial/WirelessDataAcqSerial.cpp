/*WirelessDataAcqSerial.cpp
/----------------------------------------------------------------------------
//(C) COPYRIGHT 2018 HWRacing Formula Student Team
//              ALL RIGHTS RESERVED
// Author : Bruce Thomson
//----------------------------------------------------------------------------
// Purpose: Sends ECU CAN data in the format below to a data acqusition system 
//          and the a wireless transmitter using the Serial devices and speeds provided.
//          
//          
// Requires: ECUCAN Library since the it pulls the data for the packets stright
//           from the buffers.
// Notes:   
//         
//----------------------------------------------------------------------------
*/

#include "ECUCAN.h"
#include "WirelessDataAcqSerial.h"

//Global Variables
ECUCAN         * DA_Can;
HardwareSerial * WirelessSerial;
HardwareSerial * DASerial;
long            WirelessSerialSpeed;
long            DASerialSpeed;

char           * CANFirstPacket;
char           * CANSecondPacket;
char           * CANThirdPacket;
char           * CANForthPacket;

//WirelessDataAcqSerial Constructor
//Begins the Serial communications on the two provided Serial objects
WirelessDataAcqSerial::WirelessDataAcqSerial(ECUCAN * CAN, HardwareSerial * wirelessSerial, long wirelessSerialSpeed, HardwareSerial * dASerial, long dASerialSpeed)
{
  DA_Can = CAN;
  WirelessSerial      = wirelessSerial;
  WirelessSerialSpeed = wirelessSerialSpeed; 
  DASerial            = dASerial;
  DASerialSpeed       = dASerialSpeed;

  WirelessSerial->begin(WirelessSerialSpeed);

  DASerial->begin(DASerialSpeed);

  CANFirstPacket  = DA_Can->getPacketPointer(ECU_CAN_0_IDENT);
  CANSecondPacket = DA_Can->getPacketPointer(ECU_CAN_1_IDENT);
  CANThirdPacket  = DA_Can->getPacketPointer(ECU_CAN_2_IDENT);
  CANForthPacket  = DA_Can->getPacketPointer(ECU_CAN_3_IDENT);
}


void WirelessDataAcqSerial::sendData()
{
  if (WirelessSerial != NULL)
  {
    sendDataWireless();
  }

  if (DASerial != NULL)
  {
    sendDataDA();
  }

}

void WirelessDataAcqSerial::sendDataWireless()
{
  //Packet 0x20
  WirelessSerial->write(SERIAL_HEADER_FIRST_BYTE);
  WirelessSerial->write(SERIAL_HEADER_SECOND_BYTE);
  WirelessSerial->write(SERIAL_PACKET_1_ID);
  WirelessSerial->write(CANFirstPacket, SERIAL_PACKET_LENGTH);

  //Packet 0x21
  WirelessSerial->write(SERIAL_HEADER_FIRST_BYTE);
  WirelessSerial->write(SERIAL_HEADER_SECOND_BYTE);
  WirelessSerial->write(SERIAL_PACKET_2_ID);
  WirelessSerial->write(CANSecondPacket, SERIAL_PACKET_LENGTH);

  //Packet 0x22
  WirelessSerial->write(SERIAL_HEADER_FIRST_BYTE);
  WirelessSerial->write(SERIAL_HEADER_SECOND_BYTE);
  WirelessSerial->write(SERIAL_PACKET_3_ID);
  WirelessSerial->write(CANThirdPacket, SERIAL_PACKET_LENGTH);

  //Packet 0x23
  WirelessSerial->write(SERIAL_HEADER_FIRST_BYTE);
  WirelessSerial->write(SERIAL_HEADER_SECOND_BYTE);
  WirelessSerial->write(SERIAL_PACKET_4_ID);
  WirelessSerial->write(CANForthPacket, SERIAL_PACKET_LENGTH);
}

void  WirelessDataAcqSerial::sendDataDA()
{
  //Packet 0x20
  DASerial->write(SERIAL_HEADER_FIRST_BYTE);
  DASerial->write(SERIAL_HEADER_SECOND_BYTE);
  DASerial->write(SERIAL_PACKET_1_ID);
  DASerial->write(CANFirstPacket, SERIAL_PACKET_LENGTH);

  //Packet 0x21
  DASerial->write(SERIAL_HEADER_FIRST_BYTE);
  DASerial->write(SERIAL_HEADER_SECOND_BYTE);
  DASerial->write(SERIAL_PACKET_2_ID);
  DASerial->write(CANSecondPacket, SERIAL_PACKET_LENGTH);

  //Packet 0x22
  DASerial->write(SERIAL_HEADER_FIRST_BYTE);
  DASerial->write(SERIAL_HEADER_SECOND_BYTE);
  DASerial->write(SERIAL_PACKET_3_ID);
  DASerial->write(CANThirdPacket, SERIAL_PACKET_LENGTH);

  //Packet 0x23
  DASerial->write(SERIAL_HEADER_FIRST_BYTE);
  DASerial->write(SERIAL_HEADER_SECOND_BYTE);
  DASerial->write(SERIAL_PACKET_4_ID);
  DASerial->write(CANForthPacket, SERIAL_PACKET_LENGTH);

}

void WirelessDataAcqSerial::sendDistanceWireless(uint16_t distanceKM)
{
  //Packet 0x20
  byte distance[2];
  *distance = distanceKM;
  DASerial->write(SERIAL_HEADER_FIRST_BYTE);
  DASerial->write(SERIAL_HEADER_SECOND_BYTE);
  DASerial->write(SERIAL_PACKET_5_ID);
  DASerial->write(distance, 2);
}

void WirelessDataAcqSerial::sendDistanceDA(uint16_t distanceKM)
{
  //Packet 0x20
  byte distance[2];
  *distance = distanceKM;
  DASerial->write(SERIAL_HEADER_FIRST_BYTE);
  DASerial->write(SERIAL_HEADER_SECOND_BYTE);
  DASerial->write(SERIAL_PACKET_5_ID);
  DASerial->write(distance, 2);
}