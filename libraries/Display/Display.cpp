
/*Display.cpp
/----------------------------------------------------------------------------
//(C) COPYRIGHT 2018 HWRacing Formula Student Team
//              ALL RIGHTS RESERVED
// Author : Bruce Thomson
//----------------------------------------------------------------------------
// Purpose: Wraps around the Display Functions of the Nextion
//          display. Provides functions for updating the display
//          with current values from the ECUCAN object provided.
// Requires: Modified Nextion Display Arduino Library
//----------------------------------------------------------------------------
*/
#include "Display.h"
#include "Nextion.h"
#include "ECUCAN.h"
#include "DistanceTravelled.h"

//NexPages
NexPage  InitPage            = NexPage(INIT_PAGE_ID, 0,"LOADING");
NexPage  DashPage            = NexPage(DASH_PAGE_ID, 0,"DASH");
NexPage  DiagPage            = NexPage(DIAG_PAGE_ID, 0,"DIAG");

//NexButtons Diag Screen
NexButton AeroButton         = NexButton(INIT_PAGE_ID, BUT_INIT_AERO_COMP_ID, "butAero");
NexButton ContButton         = NexButton(INIT_PAGE_ID, BUT_INIT_CONT_COMP_ID, "butCont");

//NexProgress LoadBar
NexProgressBar LoadBar       = NexProgressBar(INIT_PAGE_ID, LOADBAR_INIT_LOAD_COMP_ID, "DIAG_LOAD_SLID");

//NexText Diag Screen
NexText  Init_StatusNexText  = NexText(DIAG_PAGE_ID, BUT_INIT_STATUS_TEXT, "DIAG_STATUS");

//NexButtons Dash Screen
NexButton  WaterButton       = NexButton(DASH_PAGE_ID, BUT_DASH_WATER_COMP_ID, "butWater");
NexButton  FuelButton        = NexButton(DASH_PAGE_ID, BUT_DASH_FUEL_COMP_ID, "butFUEL");

//NexText/Num Dash Screen
NexText GearNexText          = NexText(DASH_PAGE_ID, TEXT_DASH_GEAR_COMP_ID, "GEAR");
NexNum RPMNexNum             = NexNum(DASH_PAGE_ID, NUM_DASH_RPM_COMP_ID, "RPM");
NexNum SpeedNexNum           = NexNum(DASH_PAGE_ID, NUM_DASH_SPEED_COMP_ID, "SPEED");
NexNum BatNexNum             = NexNum(DASH_PAGE_ID, NUM_DASH_BAT_COMP_ID, "BAT");
NexNum DistanceNexNum        = NexNum(DASH_PAGE_ID, NUM_DASH_DISTANCE_COMP_ID,"DISTANCE");

//NexButtons Diag Screen
NexButton CANReconnectButton = NexButton(DIAG_PAGE_ID, BUT_DIAG_CAN_COMP_ID, "butCANEnable");
NexButton ResetDistanceButton= NexButton(DIAG_PAGE_ID, BUT_DIAG_DISTANCE_COMP_ID, "butResetDist");

//NexText Diag Screen
NexText Diag_CANNexText       = NexText(DIAG_PAGE_ID, TEXT_DIAG_CAN_COMP_ID, "DIAG_CAN");
NexText Diag_GearNexText      = NexText(DIAG_PAGE_ID, TEXT_DIAG_GEAR_COMP_ID, "DIAG_GEAR");
NexNum Diag_RPMNexNum         = NexNum(DIAG_PAGE_ID, NUM_DIAG_RPM_COMP_ID, "DIAG_RPM");
NexNum Diag_TPSNexNum         = NexNum(DIAG_PAGE_ID, NUM_DIAG_TPS_COMP_ID, "DIAG_TPS");
NexNum Diag_WaterNexNum       = NexNum(DIAG_PAGE_ID, NUM_DIAG_WATER_COMP_ID, "DIAG_WATER");
NexNum Diag_AirNexNum         = NexNum(DIAG_PAGE_ID, NUM_DIAG_AIR_COMP_ID, "DIAG_AIR");
NexNum Diag_MAPNexNum         = NexNum(DIAG_PAGE_ID, NUM_DIAG_MAP_COMP_ID, "DIAG_MAP");
NexText Diag_LambdaNexText    = NexText(DIAG_PAGE_ID, TEXT_DIAG_LAMBDA_COMP_ID, "DIAG_LAMBDA");
NexText Diag_SpeedNexText     = NexText(DIAG_PAGE_ID, TEXT_DIAG_SPEED_COMP_ID, "DIAG_SPEED");
NexText Diag_BatNexText       = NexText(DIAG_PAGE_ID, TEXT_DIAG_BAT_COMP_ID, "DIAG_BAT");
NexText Diag_FuelConsNexText  = NexText(DIAG_PAGE_ID, TEXT_DIAG_FUEL_COMP_ID, "DIAG_FUEL");
NexText Diag_InjectionNexText = NexText(DIAG_PAGE_ID, TEXT_DIAG_INJECT_COMP_ID, "DIAG_INJECT");
NexNum Diag_FuelPresNexNum    = NexNum(DIAG_PAGE_ID, NUM_DIAG_FUEL_PRES_COMP_ID, "DIAG_FUEL_PRES");
NexNum Diag_OilPresNexNum     = NexNum(DIAG_PAGE_ID, NUM_DIAG_OIL_PRES_COMP_ID, "DIAG_OIL_PRES");
NexNum Diag_DistanceNexNum   = NexNum(DIAG_PAGE_ID, NUM_DIAG_DISTANCE_COMP_ID, "DIAG_DISTANCE");
NexText Diag_ECUNextText      = NexText(DIAG_PAGE_ID, TEXT_DIAG_ECU_COMP_ID, "DIAG_ECU");

//NexTough list of items to perform call backs on.
NexTouch *nex_listen_list[] = { &AeroButton, &ContButton, &CANReconnectButton, &ResetDistanceButton, NULL};

//State Variables
uint8_t DisplayCurrentPage;
uint8_t DisplayContPressed = 0;
uint8_t Display_CAN_Enabled = 0;
uint8_t screenChange = 0;
long    screenChangeISRTriggered = 0;

//ECUCAN Object Pointer
ECUCAN * DisplayCan;
DistanceTravelled * DisplayDistance;

//Init Screen Buffer Pointers and Variables
int8_t loadBarPrecentage = 0;

//Dash Screen Buffer Pointers
char GearChar[2];

//Diag Screen Buffer Pointers
char diagSpeedChar[6];
char diagLambdaChar[4];
char diagVoltageChar[5];
char diagFuelConChar[5];
char diagInjectChar[5];

uint8_t diagScreenUpdate = false;

//CONSTRUCTOR
//Display Object Constructor.
//- Initalises the communication to the screen.
//- Attaches the callback's to the required buttons
//- Then shows the initialisation page.
Display::Display(ECUCAN * CAN , DistanceTravelled * DISTANCE)
{
  DisplayCan = CAN;
  DisplayDistance = DISTANCE;
} 

//startup function displays 
void Display::startup()
{
  nexInit();
  DisplayCurrentPage = INIT_PAGE_ID;
  InitPage.show();
  LoadBar.setValue(25);
}

//getButtonsForCallbacks function attaches the callback handlers to the NexButtons
//of the page so that the system responds.
void Display::attachCallBacks()
{
  AeroButton.attachPop(aeroButtonCallBack, &AeroButton);
  ContButton.attachPop(contButtonCallBack, &ContButton);
  CANReconnectButton.attachPop(CANReconnectButtonCallBack, &CANReconnectButton);
  ResetDistanceButton.attachPop(resetDistanceCallback, &ResetDistanceButton);

}

//runDisplayLoop function runs the library's loop.
//THIS FUNCTION MUST BE CALLED WITHIN THE LOOP OF THE
//MAIN ARDUINO SKETCH
void Display::runDisplayLoop()
{
  nexLoop(nex_listen_list);
  if (screenChange > 0)
  {
    int screenSamples = 0;
    //Screen Change Debounce
    for (int i = 0; i < SWITCH_TOTAL_DEBOUNCE_POLLS; i++)
    {
      delayMicroseconds(SWITCH_DEBOUNCE_DELAY);
      
      #ifndef DEBUG
        Serial.print("Current Poll ");
        Serial.print(i, DEC);
        Serial.print(" Switch state");
        Serial.print(digitalRead(BUTTON_INT_PIN), DEC);
        Serial.print("Successful Samples ");
        Serial.println(screenSamples);
      #endif

      if (digitalRead(BUTTON_INT_PIN) == LOW)
      {
        screenSamples++;
      }
    }

    if (screenSamples >= SWITCH_ACCEPT_DEBOUNCE_POLLS)
    {
      #ifdef DEBUG
        Serial.print("Change Screen. Number of Polls");
        Serial.println(screenSamples, DEC);
      #endif
      changeDisplay();
    }

    screenChange = 0;  
  }
}

void Display::attachChangeInterrupt()
{
  pinMode(BUTTON_INT_PIN,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(BUTTON_INT_PIN),screenChangeISR,FALLING);
  #ifdef DEBUG
    Serial.println("Attached Screen Switch Button Interupt");
  #endif
}

void Display::screenChangeISR()
{
    screenChange = 1;
}

void Display::changeDisplay()
{
  switch (DisplayCurrentPage)
  {
    case(DASH_PAGE_ID):
      DiagPage.show();
      DisplayCurrentPage = DIAG_PAGE_ID;
      break;
    case(DIAG_PAGE_ID):
      DashPage.show();
      DisplayCurrentPage = DASH_PAGE_ID;
      break;
    default:
      DashPage.show();
      DisplayCurrentPage = DASH_PAGE_ID;
  }
  updateDistance(DisplayDistance->getDistanceKMInteger());
}

void Display::sendCurrentDisplay()
{
  switch(DisplayCurrentPage)
  {
    case (INIT_PAGE_ID):
      InitPage.show();
      break;
    case (DIAG_PAGE_ID):
      DiagPage.show();
      break;
    default:
      DashPage.show();
  }
  updateDistance(DisplayDistance->getDistanceKMInteger());
}

//updateValues function calls the correct updateScreen
//function within the class to update the correct values
void Display::updateValues()
{
  switch(DisplayCurrentPage)
  {
    case (INIT_PAGE_ID):
      updateInitScreen();
      break;
    case (DASH_PAGE_ID):
      updateDashScreen();
      break;
    case (DIAG_PAGE_ID):
      if (diagScreenUpdate == 4)
      {
        updateDiagScreen();
        diagScreenUpdate = 0;
      }
      diagScreenUpdate++;
      break;
  }
}

//getCurrentDisplay function returns the defined
//ID of the current page displayed to the user.
uint8_t Display::getCurrentPage()
{
  return DisplayCurrentPage;
}

void Display::setCANStatus(uint8_t enabled)
{
  Display_CAN_Enabled = enabled;

}

void Display::CANStarted()
{ 
  LoadBar.setValue(50);
  LoadBar.setForebarColor(LIGHT_GREEN);
  Init_StatusNexText.setText("CAN Initialising");
}
void Display::CANFinished()
{
  LoadBar.setValue(70);
  LoadBar.setForebarColor(LIGHT_GREEN);
  Init_StatusNexText.setText("Connecting to ECU");
}

void Display::setupComplete()
{
  if ((Display_CAN_Enabled == 1) && (DisplayCan->validPacketReceivedSinceStartup()))
  {
    Init_StatusNexText.setText("Setup Complete");
    LoadBar.setValue(100);
    LoadBar.setForebarColor(LIGHT_GREEN);
    delay(500);
    DisplayCurrentPage = DASH_PAGE_ID;
    DashPage.show();
  }
  else if ((Display_CAN_Enabled == 1) && (DisplayCan->validPacketReceivedSinceStartup() == 0))
  {
    Init_StatusNexText.setText("CAN Init. Not Connected to ECU");
    LoadBar.setValue(100);
    LoadBar.setForebarColor(RED);
  }
  else
  {
    Init_StatusNexText.setText("CAN Failed. Not Connected to ECU");
    LoadBar.setValue(100);
    LoadBar.setForebarColor(RED);
  }
}

static void Display::resetDistanceCallback(void *ptr)
{
  DisplayDistance->reset();
  uint16_t distanceKm = DisplayDistance->getDistanceKMInteger();
  Diag_DistanceNexNum.setValue(distanceKm);
}

//aeroButtonCallBack function provides giggles. 
static void Display::aeroButtonCallBack(void *ptr)
{
  Init_StatusNexText.setText("This feature is not avaliable");
  interrupts();
}

//contButtonCallBack function provides call back
//for the user pressing the continue button on the diag screen
static void Display::contButtonCallBack(void *ptr)
{
  DisplayCurrentPage = DIAG_PAGE_ID;
  DiagPage.show();
  interrupts();
}

//CANReconnectButtonCallBack performs the connecting 
static void Display::CANReconnectButtonCallBack(void *ptr)
{
  DisplayCan->init();
  DisplayCan->enableInterrupt(2);
  interrupts();
}

//convertGear function returns the char value for the Gear
//int provided. Will return N if the car is in Neutral.
char * Display::convertGear(short gear)
{
  itoa(gear, GearChar, 10);
  if (GearChar[0] == '0')
  {
    GearChar[0] = 'N';
  }
  return GearChar;
}

//convert1000xIntToChar function converts a short data value
//which is multiplied by 1000 to char equivalent with the decimal
//point in the correct place.
char * Display::convert1000xIntToChar(short value, char * charArray)
{
  char * currentPointer = &charArray[0];
  short workingValue = value;

  uint8_t Hundreds  = workingValue / 1000;
  workingValue      = workingValue % 1000;
  uint8_t Tens      = workingValue / 100;
  workingValue      = workingValue % 100;
  uint8_t Digits    = workingValue / 10;
  uint8_t Tenths    = workingValue % 10;

  itoa(Hundreds,currentPointer, 10);
  currentPointer++;
  *currentPointer = '.';
  currentPointer++;
  itoa(Tens,currentPointer, 10);
  currentPointer++;
  itoa(Digits,currentPointer, 10);
  currentPointer++;
  itoa(Tenths,currentPointer, 10);

  return charArray;
}


//convert10xIntToChar function converts a short data value
//which is multiplied by 10 to char equivalent with the decimal
//point in the correct place.
char * Display::convert10xIntToChar(short value, char * charArray)
{
  char * currentPointer = &charArray[0];
  char * previousPointer = &charArray[0] ;

  uint8_t Hundreds = value / 1000;
  value = value % 1000;
  uint8_t Tens     = value / 100;
  value = value % 100;
  uint8_t Digits = value / 10;
  uint8_t Tenths = value % 10;

  itoa(Hundreds,currentPointer, 10);
  if (*currentPointer != '0')
  {
    previousPointer = currentPointer;
    currentPointer++;
  }
  itoa(Tens,currentPointer, 10);
  if ((*currentPointer != '0') && (*previousPointer != '0'))
  {
    previousPointer = currentPointer; 
    currentPointer++;
  }
  itoa(Digits,currentPointer, 10);
  currentPointer++;
  *currentPointer = '.';
  currentPointer++;
  itoa(Tenths,currentPointer, 10);

  return charArray;
}


void Display::updateInitScreen()
{
}

//updateDashScreen function updates the dash screen by
//taking in the reference to the DisplayCan Object. 
void Display::updateDashScreen()
{
  GearNexText.setText(convertGear(DisplayCan->getGear()));
  RPMNexNum.setValue(DisplayCan->getRPM());
  SpeedNexNum.setValue(DisplayCan->getSpeed()>>4);
  BatNexNum.setValue(DisplayCan->getBatteryVoltage()/10);

  int waterTemp = DisplayCan->getWaterTemp();
  int fuelPress  = DisplayCan->getFuelPress();
  
  if (waterTemp >= WATER_TEMP_WARN)
  {
    WaterButton.setBackgroundColor(RED);
  }
  else
  {
    WaterButton.setBackgroundColor(LIGHT_GREEN);
  }

  if (fuelPress >= FUEL_PRESS_WARN)
  {
    FuelButton.setBackgroundColor(RED);
  }
  else
  {
    FuelButton.setBackgroundColor(LIGHT_GREEN);
  }
}

//Method which updates the distance value on the screen.
void Display::updateDistance(uint16_t distanceKm)
{
  switch (DisplayCurrentPage)
  {
    case (DASH_PAGE_ID):
      DistanceNexNum.setValue(distanceKm);
      break;
    case (DIAG_PAGE_ID):
      Diag_DistanceNexNum.setValue(distanceKm);
      break;
  }

}

//updateDiagScreen function updates the diag screen
//by taking in the reference to the DisplayCan Object
void Display::updateDiagScreen()
{
  convert1000xIntToChar(DisplayCan->getLambda(), diagLambdaChar);
  convert10xIntToChar(DisplayCan->getSpeed(), diagSpeedChar);
  convert10xIntToChar(DisplayCan->getBatteryVoltage(), diagVoltageChar);
  convert10xIntToChar(DisplayCan->getFuelConLitresPerHour(), diagFuelConChar);
  itoa(DisplayCan->getInjectionMs(), diagInjectChar, 10);

  if (Display_CAN_Enabled)
  {
    Diag_CANNexText.setText("Enabled");
  }
  else
  {
    Diag_CANNexText.setText("Disabled");
  }

  Diag_ECUNextText.setText("Connected");
  Diag_GearNexText.setText(convertGear(DisplayCan->getGear()));
  Diag_RPMNexNum.setValue(DisplayCan->getRPM());
  Diag_TPSNexNum.setValue(DisplayCan->getTPS());
  Diag_WaterNexNum.setValue(DisplayCan->getWaterTemp());
  Diag_AirNexNum.setValue(DisplayCan->getAirTemp());
  Diag_MAPNexNum.setValue(DisplayCan->getMAP());
  Diag_LambdaNexText.setText(diagLambdaChar);
  Diag_SpeedNexText.setText(diagSpeedChar);
  Diag_BatNexText.setText(diagVoltageChar);
  Diag_FuelConsNexText.setText(diagFuelConChar);
  Diag_InjectionNexText.setText(diagInjectChar);
  Diag_FuelPresNexNum.setValue(DisplayCan->getFuelPress());
  Diag_OilPresNexNum.setValue(DisplayCan->getOilPress());

}

void Display::infoWarning()
{
  switch (DisplayCurrentPage)
  {
    case (DASH_PAGE_ID):
    {
      GearNexText.setText("-");
      RPMNexNum.setValue(0);
      SpeedNexNum.setValue(0);
      BatNexNum.setValue(0);
      WaterButton.setBackgroundColor(WHITE);
      FuelButton.setBackgroundColor(WHITE);
      break;
    }
    case (DIAG_PAGE_ID):
    {
      if (Display_CAN_Enabled)
      {
        Diag_CANNexText.setText("Enabled");
      }
      else
      {
        Diag_CANNexText.setText("Disabled");
      }

      Diag_ECUNextText.setText("Disconnected");

      Diag_GearNexText.setText("-");
      Diag_RPMNexNum.setValue(0);
      Diag_TPSNexNum.setValue(0);
      Diag_WaterNexNum.setValue(0);
      Diag_AirNexNum.setValue(0);
      Diag_MAPNexNum.setValue(0);
      Diag_LambdaNexText.setText("-");
      Diag_SpeedNexText.setText("-");
      Diag_BatNexText.setText("-");
      Diag_FuelConsNexText.setText("-");
      Diag_InjectionNexText.setText("-");
      Diag_FuelPresNexNum.setValue(0);
      Diag_OilPresNexNum.setValue(0);
      break;
    }
  }
}