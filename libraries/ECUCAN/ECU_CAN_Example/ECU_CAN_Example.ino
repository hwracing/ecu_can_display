/*ECUCAN Example Sketch
/----------------------------------------------------------------------------
//(C) COPYRIGHT 2017-2018 HWRacing Formula Student Team
//              ALL RIGHTS RESERVED
// Author : Bruce Thomson
//----------------------------------------------------------------------------
// Purpose: Accepts DTAFast ECU CAN packets and will perform the LSB (Least Significant Byte) 
//          to MSB (Most Significant Byte) conversion. Acts as a wrapper library. Accessor methods
//          are given for each data field of the DTAFAst CAN Stream.
//          See https://www.dtafast.co.uk/download_files/Manuals/S%20Series%20Manual.pdf for details
//          of CAN Stream
// Requires: SeeedStudio Arduino CAN Shield Library (mcp_can.h,mcp_can.cpp, mcp_can_defs.h)
//----------------------------------------------------------------------------
*/
#include "ECUCAN.h"

#define CAN_CS_PIN 53
#define CAN_INTERRUPT_PIN 2

ECUCAN can(CAN_CS_PIN,&SPI);

void setup()
{
  Serial.begin(115200);
  if (can.init())
  {
    Serial.println("CAN ERROR");
  }
  can.setupMaskFilters(4);
  can.enableInterrupt(CAN_INTERRUPT_PIN); //When CAN interrupts are enabled there is no need to call the readFrame() method.
}

void loop()
{
  Serial.print("The Speed is ");
  Serial.print(can.getSpeed()/10, DEC);
  Serial.println(" Km/h");
  Serial.print("The Gear is :"); 
  Serial.println(can.getGear(),DEC);
  delay(100);

}