/*Display.h
/----------------------------------------------------------------------------
//(C) COPYRIGHT 2018 HWRacing Formula Student Team
//              ALL RIGHTS RESERVED
// Author : Bruce Thomson
//----------------------------------------------------------------------------
// Purpose: Wraps around the Display Functions of the  to update the display.
// Requires: Modified Nextion Display Arduino Library
//----------------------------------------------------------------------------
*/
#ifndef _Display_h
#define _Display_h

#include "ECUCAN.h"
#include "DistanceTravelled.h"
#include "src/ITEADLIB_1/Nextion.h"

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

#define BUTTON_INT_PIN               3
#define BUTTON_POLL_DELAY            40
#define BUTTON_POLL_NUM              4

#define FUEL_PRESS_WARN              200
#define WATER_TEMP_WARN              110
#define BATTERY_VOLTAGE_WARN		 124 //12.5V Battery Voltage Warning

#define DASH_UPDATE_DELAY            40
#define FUEL_PRESS_WARN              500
#define WATER_TEMP_WARN              50
#define DASH_UPDATE_DELAY            40 //milliseconds

#define SWITCH_DEBOUNCE_DELAY        250 //microseconds. Witnessed debounce of switches were around about 50uS each.
#define SWITCH_TOTAL_DEBOUNCE_POLLS  10
#define SWITCH_ACCEPT_DEBOUNCE_POLLS 8


#define INIT_PAGE_ID                0
#define DASH_PAGE_ID                1
#define DIAG_PAGE_ID                2

#define BUT_INIT_AERO_COMP_ID       5
#define BUT_INIT_CONT_COMP_ID       3
#define LOADBAR_INIT_LOAD_COMP_ID   4
#define BUT_INIT_STATUS_TEXT        2

#define BUT_DASH_WATER_COMP_ID      7
#define BUT_DASH_FUEL_COMP_ID       8
#define BUT_DASH_BATTERY_COMP_ID	14

#define NUM_DASH_BAT_COMP_ID       12
#define TEXT_DASH_GEAR_COMP_ID     3
#define NUM_DASH_RPM_COMP_ID       10
#define NUM_DASH_SPEED_COMP_ID     1
#define NUM_DASH_DISTANCE_COMP_ID  13

#define BUT_DIAG_CAN_COMP_ID        15
#define BUT_DIAG_DISTANCE_COMP_ID   33

#define TEXT_DIAG_CAN_COMP_ID       2
#define TEXT_DIAG_GEAR_COMP_ID      21
#define NUM_DIAG_RPM_COMP_ID        22
#define NUM_DIAG_TPS_COMP_ID        23
#define NUM_DIAG_WATER_COMP_ID      24
#define NUM_DIAG_AIR_COMP_ID        25
#define NUM_DIAG_MAP_COMP_ID        26
#define TEXT_DIAG_LAMBDA_COMP_ID    34
#define TEXT_DIAG_SPEED_COMP_ID     16
#define TEXT_DIAG_BAT_COMP_ID       17
#define TEXT_DIAG_FUEL_COMP_ID      19
#define TEXT_DIAG_INJECT_COMP_ID    18
#define NUM_DIAG_FUEL_PRES_COMP_ID  27
#define NUM_DIAG_OIL_PRES_COMP_ID   28
#define NUM_DIAG_DISTANCE_COMP_ID   30
#define TEXT_DIAG_ECU_COMP_ID       33

static void resetDistanceCallback(void *ptr);
static void aeroButtonCallBack(void *ptr);
static void dashButtonCallBack(void *ptr);
static void contButtonCallBack(void *ptr);
static void CANReconnectButtonCallBack(void *ptr);
static void diagButtonCallBack(void *ptr);

class Display
{
  public:

  Display(ECUCAN * CAN);

  void startup();
  void attachCallBacks();
  void attachChangeInterrupt();
  void runDisplayLoop();
  void updateValues();
  void updateDistance(uint16_t distanceKm); 
  uint8_t getCurrentPage();
  void setCANStatus(uint8_t enabled);
  void CANStarted();
  void CANFinished();
  void setupComplete();
  void ChangeInterrupt();
  static void screenChangeISR();
  void changeDisplay();
  void infoWarning();
  void sendCurrentDisplay();

  private:
  char * convertGear(short gear);
  char * convert1000xIntToChar(short value, char * charArray);
  char * convert10xIntToChar(short value, char * charArray);
  void updateInitScreen();
  void updateDashScreen();
  void updateDiagScreen();
};

#endif