﻿/*Begining of Auto generated code by Atmel studio */
#include <Arduino.h>

/*End of auto generated code by Atmel studio */

#include "src/EERPOM/EEPROM.h"
#include "display_ecu_wireless_defs.h"
#include "ECUCAN.h"
#include "Display.h"
#include "src/ITEADLIB_1/Nextion.h"
#include "src/SoftReset/SoftReset.h"
//Beginning of Auto generated function prototypes by Atmel Studio
uint8_t initCAN();
void CANLoop();
bool isScreenConnected();
//End of Auto generated function prototypes by Atmel Studio



//ECUCAN DTA Fast Wrapper Library
ECUCAN Can(CAN_CS_PIN,&SPI);

//DistanceTravelled Wrapper Library
//DistanceTravelled Distance(5);

//Display Wrapper Library. Takes in the CAN Object so it
//to access the fields of the CAN packet.
Display Display(&Can);


//Previous Millis Time. Used for multitasking
unsigned long previousDisplayTime = 0;
unsigned long previousDistanceTime = 0;
unsigned long previousWirelessTime = 0;
unsigned long previousDATime      = 0;
unsigned long currentTime         = 0;

//Boolean to hold the value of the screen
bool prevScreenConnected = false;


// initCAN function calls the initalisation code
// on the MCP2515 and setup the interrupt and filters.
uint8_t initCAN()
{
  Display.CANStarted();
  if (Can.init())
  {
    #ifdef DEBUG
      Serial.println("CAN DISABLED");
    #endif

    Display.setCANStatus(0);
    return 0;
  }
  
  
  Can.setupMaskFilters(1);
  Display.setCANStatus(1);

  Can.enableInterrupt(CAN_INTERRUPT_PIN);

  Display.CANFinished();
}

void CANLoop()
{
  //Wait for the First CAN packet to come in
  volatile long currentTime = millis();
  volatile long lastTime = currentTime;
  volatile uint8_t CAN_ConnectionAttempts = 0;

  while (CAN_ConnectionAttempts < 10)
  {
    currentTime = millis();
    if (currentTime >  lastTime + 100)
    {
      initCAN();
      lastTime = currentTime;
      CAN_ConnectionAttempts++;
    }
    
    if (Can.validPacketReceivedSinceStartup() == 1)
    {
      Serial.print("CAN ATTEMPTS");
      Serial.println(CAN_ConnectionAttempts, DEC);
      break;
    }
  }

  uint8_t numberOfAttemptsToConnectECU = EEPROM.read(1);

  if (Can.validPacketReceivedSinceStartup() == 0 && numberOfAttemptsToConnectECU < 4)
  {
	Serial.print("Restarting");
    numberOfAttemptsToConnectECU++;
    EEPROM.write(1,numberOfAttemptsToConnectECU);
    soft_restart();
  }
  else
  {
    EEPROM.write(1,0);
  }
}

bool isScreenConnected()
{
  bool value = digitalRead(SCREEN_DETECTION_PIN);
  return value;
}
//Arduino Setup function 
void setup()
{
  //Setup the ScreenConnected detection pin
  pinMode(SCREEN_DETECTION_PIN ,INPUT);
  pinMode(SCK, OUTPUT); 

  #ifdef DEBUG
    Serial.begin(115200);
	while (!Serial) ; // Need to wait on the Arduino Micro
    Serial.println("Serial Debugging Enabled at 115200 Baud");
  #endif

  while (!isScreenConnected())
  {
    Serial.println("Screen is not connected");
    delay(100); //Check again in 100ms
  }

  Display.startup();
  //Setup the Callbacks for the buttons on Screen
  Display.attachCallBacks();

  //Initialises CAN and tells the screen if CAN is working.
  CANLoop();
  
  //setupComplete moves the display from the INIT_PAGE to 
  //the DASH_PAGE if CAN is successful. However, will display
  //error messages.

  Display.setupComplete();
  Display.attachChangeInterrupt();
  //Distance.init();

  prevScreenConnected = isScreenConnected();
}

//Arduino Loop function 
void loop()
{
  if (isScreenConnected() && prevScreenConnected)
  {
    //Can.readPacket();
    //get the current time so we can enter the loop;
    currentTime = millis();

    //Handle Any Display Callbacks
    Display.runDisplayLoop();

    //Update The Display's Values
    if (currentTime - previousDisplayTime > SCREEN_UPDATE_INTERVAL)
    { 
      #ifdef DEBUG
        Serial.print("Update Display Triggered at:");
        Serial.println("");
        Serial.print("CAN GEAR");
        Serial.println(Can.getGear(),DEC);
	
		Serial.print("Voltage ");
		Serial.println(Can.getBatteryVoltage(), DEC);
		
		
    //  Serial.println(currentTime,DEC);
      #endif
      //Update when the last CAN packet received is within an acceptable level. Otherwise display warning to Driver.
      if (currentTime > Can.timeOfLastPacketReceived() + CAN_INFO_WARN_DELAY)
      {
        Display.infoWarning();
      }
      else
      {
        Display.updateValues();
      }
      previousDisplayTime = currentTime;
      currentTime = millis();
    }

    //Update Distance. Store the values and send distance data to DA and Wireless
    if (currentTime - previousDistanceTime > DISTANCE_UPDATE_INTERVAL)
    {
	   for (uint8_t i = 0; i < 10; i++)
	   {
			Serial.print("P");
			Serial.print(i,DEC);
			Serial.print(" ");
			Serial.println(Can.ValidPackets()[i],DEC);
			Can.ValidPackets()[i] = false;
	   }
		
		
      Serial.print("Update Distance");
      uint16_t distanceTravelled = 54;
     // Distance.writeDistance();
      Serial.println(distanceTravelled,DEC);
      Display.updateDistance(distanceTravelled);
    
      previousDistanceTime = currentTime;
      currentTime = millis();
    }
  }
  else if (!isScreenConnected())
  {
    #ifdef DEBUG
      Serial.println("Screen not connected");
    #endif
    prevScreenConnected = false;
  }
  else if ((isScreenConnected()) && (!prevScreenConnected)) 
  {
    delay(1000);
    prevScreenConnected = true;
    Display.sendCurrentDisplay();
  }

}
