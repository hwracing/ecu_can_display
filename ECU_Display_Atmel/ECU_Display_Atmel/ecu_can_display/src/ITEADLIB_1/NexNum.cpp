/**
 * @file NexNum.cpp
 *
 * The implementation of class NexNum. 
 *
 * @author  Bruce Thomson
 * @date    2015/8/13
 * @copyright 
 * Copyright (C) 2018 HWRacing Formula Student Team
 */
#include "NexNum.h"

NexNum::NexNum(uint8_t pid, uint8_t cid, const char *name)
    :NexTouch(pid, cid, name)
{
}

bool NexNum::getValue(uint32_t *number)
{
    String cmd = String("get ");
    cmd += getObjName();
    cmd += ".val";
    sendCommand(cmd.c_str());
    return recvRetNumber(number);
}

bool NexNum::setValue(uint32_t number)
{
    char buf[10] = {0};
    String cmd;
    
    utoa(number, buf, 10);
    cmd += getObjName();
    cmd += ".val=";
    cmd += buf;

    sendCommand(cmd.c_str());
    return recvRetCommandFinished();
}

bool NexNum::setBackgroundColor(uint16_t color)
{
    char buf[10] = {0};
    String cmd;
    
    utoa(color, buf, 10);
    cmd += getObjName();
    cmd += ".bco=";
    cmd += buf;

    sendCommand(cmd.c_str());
    return recvRetCommandFinished();
}

bool NexNum::setNumberColor(uint16_t color)
{
    char buf[10] = {0};
    String cmd;
    
    utoa(color, buf, 10);
    cmd += getObjName();
    cmd += ".pco=";
    cmd += buf;

    sendCommand(cmd.c_str());
    return recvRetCommandFinished();
}


