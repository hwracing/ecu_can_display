
#include "mbed.h"
#include "ArduinoSPI.h"
#include "ArduinoSerial.h"
extern ArduinoSerial1 ArduinoSerialPC;
extern DigitalOut			mbedCANSPICS;
extern SPI 						mbedCANSPI;
extern Serial 				mbedSerialPC;

ArduinoSPI::ArduinoSPI(SPI * spi, DigitalOut CS)
{
	mbedSPI = spi;
	chipSelect = &CS;
	
	mbedCANSPI.format(8,0);
	mbedCANSPI.frequency(5000000);
}

void ArduinoSPI::begin()
{
	ArduinoSerialPC.println("SPI begin");
	mbedCANSPICS = 1;
}


void ArduinoSPI::usingInterrupt(uint8_t interruptNumber)
{
	
}

void ArduinoSPI::notUsingInterrupt(uint8_t interruptNumber)
{
	
}

void ArduinoSPI::beginTransaction(ArduinoSPISettings settings)
{
	mbedCANSPI.frequency(settings.clock);
	
	mbedCANSPI.format(8,settings.dataMode); 
	
	mbedCANSPI.lock();
	
}

void ArduinoSPI::endTransaction(void)
{
	mbedCANSPICS = 1;
	mbedCANSPI.unlock();
}


void ArduinoSPI::end()
{
		mbedCANSPICS = 1;
		mbedCANSPI.unlock();
}

inline void ArduinoSPI::setDataMode(uint8_t dataMode)
{
		mbedCANSPI.format(8,dataMode); 
}

unsigned char ArduinoSPI::transfer(unsigned char data)
{
	unsigned char returnvalue = 0;

	//mbedSerialPC.printf("BT:");
	//mbedSerialPC.printf("%x\n\r",data);
	returnvalue = mbedCANSPI.write(data);
	//mbedSerialPC.printf("AT:");
	//mbedSerialPC.printf("%x\n\r",returnvalue);
	
	return returnvalue;
}


void ArduinoSPI::transfer(void *buf, int count)
{	

}


