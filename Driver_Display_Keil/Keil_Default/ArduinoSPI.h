#ifndef ARDUINOSPI_h
#define ARDUINOSPI_h

#include "mbed.h"



class ArduinoSPISettings
{
	public:
		 uint32_t clock;
	   uint8_t 	bitOrder;
		 uint8_t  dataMode;
		   
		 ArduinoSPISettings(uint32_t Pclock, uint8_t PbitOrder, uint8_t PdataMode) 
		 {
			 clock 		= Pclock;
			 bitOrder = PbitOrder;
			 dataMode = PdataMode;
		 }
};


class ArduinoSPI
{
	public:
		SPI 				*mbedSPI;
		DigitalOut 	*chipSelect;
	
		ArduinoSPI(SPI *spi, DigitalOut CS);
		void begin();
		void usingInterrupt(uint8_t interruptNumber);
		void notUsingInterrupt(uint8_t interruptNumber);
	
		inline void beginTransaction(ArduinoSPISettings settings);
	  inline void endTransaction(void);
		void 			 end();
		unsigned char transfer (unsigned char data);
		inline void setDataMode(uint8_t dataMode);

		void transfer(void *buf, int count);
	
};

#endif
