
#include "ArduinoSerial.h"

DigitalOut flick2(LED2);
extern Serial	mbedSerialPC;
extern Serial mbedSerial1;
extern Serial mbedSerial2;


void pcSerialEvent()
{
	mbedSerialPC.getc();
}

void Serial1Event()
{
	mbedSerial1.getc();
}

void Serial2Event()
{
	mbedSerial2.getc();
}

void Serial3Event()
{
	
}

ArduinoSerial1::ArduinoSerial1(Serial * serialObject,PinName txPin)
{
	mbedSerial = serialObject;
}

void ArduinoSerial1::begin(int speed)
{
	mbedSerial->baud(speed);
	// Setup Interrupts
	
	if (txPin == USBTX)
	{
		mbedSerial->attach(&pcSerialEvent);
	}
	else if (txPin == p9)
	{
		mbedSerial->attach(&Serial1Event);
	}
	else if (txPin == p13)
	{
		mbedSerial->attach(&Serial2Event);
	}
	else if (txPin == p28)
	{
		mbedSerial->attach(&Serial3Event);
	}
	else
	{
		//Don't attach the interrupt
	}
}


void ArduinoSerial1::print(const char * string)
{
	mbedSerial->printf("%s", string);
	
	flick2 = !flick2;
}

void ArduinoSerial1::print(char c)
{
	mbedSerial->printf("%c", c);
	
	flick2 = !flick2;
}

void ArduinoSerial1::print(unsigned char b,char * format)
{
	mbedSerial->printf((char *) format, b);
	
	flick2 = !flick2;
}

void ArduinoSerial1::print(int n, char * format)
{
	mbedSerial->printf((char *) format, n);
	
	flick2 = !flick2;
}

void ArduinoSerial1::print(unsigned int n, char * format)
{
	mbedSerial->printf((char *) format, n);
	
	flick2 = !flick2;
}

void ArduinoSerial1::print(long n, char * format)
{
	mbedSerial->printf((char *) format, n);
	
	flick2 = !flick2;
}

void ArduinoSerial1::print(unsigned long n, char * format)
{
	mbedSerial->printf(format, n);
	
	flick2 = !flick2;
}

void ArduinoSerial1::println(const char * string)
{
	mbedSerial->printf("%s", string);
	
	flick2 = !flick2;
}	

void ArduinoSerial1::println(char c)
{
	mbedSerial->printf("%c", c);
	
	flick2 = !flick2;
}

void ArduinoSerial1::println(unsigned char b,char * format)
{
	mbedSerial->printf((char *) format, b);
	
	flick2 = !flick2;
}

void ArduinoSerial1::println(int n, char * format)
{
	mbedSerial->printf((char *) format, n);
	
	flick2 = !flick2;
}

void ArduinoSerial1::println(unsigned int n, char * format)
{
		mbedSerial->printf((char *) format, n);
	
	flick2 = !flick2;
}

void ArduinoSerial1::println(long n, char * format)
{
		mbedSerial->printf((char *) format, n);
	
	flick2 = !flick2;
}

void ArduinoSerial1::println(unsigned long n, char * format)
{
		mbedSerial->printf(format, n);
	
	flick2 = !flick2;
}

void ArduinoSerial1::println(double n, char * format)
{
		mbedSerial->printf(format, n);
	
	flick2 = !flick2;
}


int ArduinoSerial1::available()
{
	return mbedSerial->readable();
}

void ArduinoSerial1::setTimeout(unsigned long _timeout)
{
	timeout = _timeout;
}

void ArduinoSerial1::write(unsigned char c)
{
	mbedSerial->putc(c);
}

char ArduinoSerial1::read()
{
	return mbedSerial->getc();
}


int ArduinoSerial1::readBytes(char * buffer, int number)
{
	int counter = 0;
	
	while ((counter < number) && (mbedSerial->readable()))
	{
		buffer[counter] = mbedSerial->getc();
		counter++;
	}		
	

	return counter;
	
}

