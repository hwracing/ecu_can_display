/**
 * @example CompText.ino
 *
 * @par How to Use
 * Show how to use API of class NexText.  
 *
 * @author  Wu Pengfei (email:<pengfei.wu@itead.cc>)
 * @date    2015/7/10
 * @copyright 
 * Copyright (C) 2014-2015 ITEAD Intelligent Systems Co., Ltd. \n
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 */

#include "Nextion.h"

void t0PopCallback(void *ptr);
void b0PopCallback(void *ptr);
void b1PopCallback(void *ptr);

NexPage p0 = NexPage(0,0,"page1");
NexPage p1 = NexPage(1,0,"page0");

NexText t0 = NexText(1, 1, "t0");
NexButton b0 = NexButton(1, 2, "b0");
NexButton b1 = NexButton(1, 3, "b1");

char buffer[100] = {0};

NexTouch *nex_listen_list[] = 
{
    &t0,
    &b0,
    &b1,
    NULL
};

void t0PopCallback(void *ptr)
{
    dbSerialPrintln("t0PopCallback");
    t0.setText("50");
}

void b0PopCallback(void *ptr)
{
    uint16_t len;
    uint16_t number;
    
    dbSerialPrintln("b0PopCallback");

    memset(buffer, 0, sizeof(buffer));
    t0.getText(buffer, sizeof(buffer));
    
    number = atoi(buffer);
    number += 1;

    memset(buffer, 0, sizeof(buffer));
    itoa(number, buffer, 10);
    
    t0.setText(buffer);
}

void b1PopCallback(void *ptr)
{
    uint16_t len;
    uint16_t number;
    
    dbSerialPrintln("b1PopCallback");

    memset(buffer, 0, sizeof(buffer));
    t0.getText(buffer, sizeof(buffer));
    
    number = atoi(buffer);
    number -= 1;

    memset(buffer, 0, sizeof(buffer));
    itoa(number, buffer, 10);
    
    t0.setText(buffer);
}

void setup(void)
{
    nexInit();
    t0.attachPop(t0PopCallback);
    b0.attachPop(b0PopCallback);
    b1.attachPop(b1PopCallback);
    dbSerialPrintln("setup done");
    delay(2000);
    p1.show();

}

void loop(void)
{
    nexLoop(nex_listen_list);
}

