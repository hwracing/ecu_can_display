/**
 * @file NexNum.h
 *
 * The definition of class NexNum. 
 *
 * @author Bruce Thomson
 * @date 2015/8/13
 *
 * @copyright 
 * Copyright (C) 2018 HWRacing Formula Student Team
 */

#ifndef __NEXNUM_H__
#define __NEXNUM_H__

#include "NexTouch.h"
#include "NexHardware.h"
/**
 * @addtogroup Component 
 * @{ 
 */

/**
 * NexSlider component. 
 */
class NexNum: public NexTouch
{
public: /* methods */
    /**
     * @copydoc NexObject::NexObject(uint8_t pid, uint8_t cid, const char *name);
     */
    NexNum(uint8_t pid, uint8_t cid, const char *name);

    /**
     * Get the value of slider. 
     * 
     * @param number - an output parameter to save the value of slider.  
     * 
     * @retval true - success. 
     * @retval false - failed. 
     */
    bool getValue(uint32_t *number);
    
    /**
     * Set the value of slider.
     *
     * @param number - the value of slider.  
     *
     * @retval true - success. 
     * @retval false - failed. 
     */
    bool setValue(uint32_t number);

    bool setBackgroundColor(uint16_t color);
    bool setNumberColor(uint16_t color);
};
/**
 * @}
 */


#endif /* #ifndef __NEXNUM_H__ */
