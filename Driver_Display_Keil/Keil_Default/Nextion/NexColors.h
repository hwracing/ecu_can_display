/**
 * @file NexColors.h
 *
 * 
 *
 * @author  Bruce Thomson
 * @date    2018/01/10
 * @copyright 
 * Copyright (C) HWRacing Formula Student Team 
 */

#define RED         59392
#define LIGHT_GREEN 2024
#define WHITE       65535
