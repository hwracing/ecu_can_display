
#include "mbed.h"
#include "ArduinoSerial.h"
#include "ArduinoSPI.h"
#include "display_ecu_wireless_defs.h"
#include "millis.h"
#include "Display.h"
#include "ECUCAN.h"

uint8_t initCAN();
void CANLoop();
bool isScreenConnected();






//WirelessDataAcqSerial Library. Sends information to the wireless modules
//and 
//WirelessDataAcqSerial WirelessData(&Can, &WIRELESS_SERIAL, WIRELESS_SPEED, &DA_SERIAL, DA_SPEED);

//DistanceTravelled Wrapper Library
//DistanceTravelled Distance(5);

//Display Wrapper Library. Takes in the CAN Object so it
//to access the fields of the CAN packet.
Display disp(2);


DigitalOut flicker(LED1);

DigitalOut ECUCANFlicker(LED3);

//mbed objects
Serial	mbedSerialPC(USBTX, USBRX);
Serial	mbedSerial1(p9,p10);
Serial  mbedSerial2(p28,p27);
SPI			mbedCANSPI(p5,p6,p7);
DigitalOut mbedCANSPICS(p8);

Ticker updateDisplayTicker();

//Arduino Wrapper Objects
ArduinoSerial1 ArduinoSerialPC(&mbedSerialPC, USBTX); 
ArduinoSerial1 ArduinoSerialDisplay(&mbedSerial1, p9);
ArduinoSPI 		 ArduinoCANSPI(&mbedCANSPI, mbedCANSPICS);

//Digital Inputs
DigitalIn 	screenDetectionPin(SCREEN_DETECTION_PIN);

InterruptIn CANSPIInterruptPin(CAN_INTERRUPT_PIN);

//ECUCAN DTA Fast Wrapper Library
ECUCAN ECU_CAN(CAN_CS_PIN,&ArduinoCANSPI);

//Previous Millis Time. Used for multitasking
unsigned long previousDisplayTime = 0;
unsigned long previousDistanceTime = 0;
unsigned long previousWirelessTime = 0;
unsigned long previousDATime      = 0;
unsigned long currentTime         = 0;

//Boolean to hold the value of the screen

bool prevScreenConnected = false;


// initCAN function calls the initalisation code
// on the MCP2515 and setup the interrupt and filters.
uint8_t initCAN()
{
	
  disp.CANStarted();
  if (ECU_CAN.init())
  {
    #ifdef DEBUG
      ArduinoSerialPC.println("CAN DISABLED");
    #endif

    disp.setCANStatus(0);
    return 0;
  }
  
  
  ECU_CAN.setupMaskFilters(1);
  disp.setCANStatus(1);

  ECU_CAN.enableInterrupt(&CANSPIInterruptPin);

  disp.CANFinished();

	return 1;
	
}

void CANLoop()
{
  //Wait for the First CAN packet to come in
  volatile long currentTime = millis();
  volatile long lastTime = currentTime;
  volatile uint8_t CAN_ConnectionAttempts = 0;

  while (CAN_ConnectionAttempts < 10)
  {
    currentTime = millis();
    if (currentTime >  lastTime + 100)
    {
			ArduinoSerialPC.println("Before CAN Init");
      initCAN();
			ArduinoSerialPC.println("After CAN Init");
      lastTime = currentTime;
      CAN_ConnectionAttempts++;
    }
    
    if (ECU_CAN.validPacketReceivedSinceStartup() == 1)
    {
      ArduinoSerialPC.print("CAN ATTEMPTS");
      ArduinoSerialPC.println(CAN_ConnectionAttempts, DEC);
      break;
    }
  }

}

bool isScreenConnected()
{
  bool value = screenDetectionPin.read();
  return value;
}

//Arduino Setup function 
void setup()
{
	// Need to call millis setup to setup the mbed's systick timer
	// to give similar performance to Arduino millis function
	millisStart();
	mbedSerialPC.printf("Starting Up\n\r");
	
  //Setup the ScreenConnected detection pin
  screenDetectionPin.mode(PullNone);

  //#ifdef DEBUG
  ArduinoSerialPC.begin(115200);
  //#endif
	
  while (!isScreenConnected())
  {
    ArduinoSerialPC.println("Screen is not connected");
		flicker = !flicker;
    wait_ms(100); //Check again in 100ms
  }
	 
	ArduinoSerialPC.println("Before Screen startup");
	disp.startup();
	ArduinoSerialPC.println("Startup");

  //Setup the Callbacks for the buttons on Screen
  
	disp.attachCallBacks();
	ArduinoSerialPC.println("Attached Callbacks");
	
  //Initialises CAN and tells the screen if CAN is working.
  CANLoop();
  
  //setupComplete moves the display from the INIT_PAGE to 
  //the DASH_PAGE if CAN is successful. However, will display
  //error messages.

  disp.setupComplete();
  disp.attachChangeInterrupt();

  prevScreenConnected = isScreenConnected();
	
	//wait_ms(5000);
	//disp.changeDisplay();
	
}


//Arduino Loop function 
void loop()
{
  if (isScreenConnected() && prevScreenConnected)
  {
    //Can.readPacket();
    //get the current time so we can enter the loop;
    currentTime = millis();

    //Handle Any Display Callbacks
    disp.runDisplayLoop();

    //Update The Display's Values
    if (currentTime - previousDisplayTime > SCREEN_UPDATE_INTERVAL)
    { 
			ECU_CAN.readPacketTest();
      #ifdef DEBUG
        ArduinoSerialPC.print("Update Display Triggered at:");
        ArduinoSerialPC.println("");
        ArduinoSerialPC.print("CAN GEAR");
        //ArduinoSerialPC.println(Can.getGear(),DEC);
				//ArduinoSerialPC.println(currentTime,DEC);
      #endif
      //Update when the last CAN packet received is within an acceptable level. Otherwise display warning to Driver.
      //if (currentTime > Can.timeOfLastPacketReceived() + CAN_INFO_WARN_wait_ms)
	    //{
        //Display.infoWarning();
      //}
      //else
      //{
        //Display.updateValues();
      //}
			disp.updateValues();
      previousDisplayTime = currentTime;
      currentTime = millis();
    }
	
  }
  else if (!isScreenConnected())
  {
    #ifdef DEBUG
      ArduinoSerialPC.println("Screen not connected");
    #endif
    prevScreenConnected = false;
  }
  else if ((isScreenConnected()) && (!prevScreenConnected)) 
  {
    wait_ms(1000);
    prevScreenConnected = true;
    //Display.sendCurrentDisplay();
  }

}
	
// Trick for mbed
int main()
{
		setup();

		for (;;)
		{
			loop();
		}
		
		return 0;
}

