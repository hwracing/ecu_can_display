
#ifndef _ARDUINO_SERIAL1_H
#define _ARDUINO_SERIAL1_H

#include "mbed.h"

#define DEC 	"%d"
#define FLOAT	'%f'
#define HEX		'%f'
#define OCT 	'%o'



class ArduinoSerial1
{
  public:
		ArduinoSerial1(Serial * serialObject,PinName txPin);
		void begin(int speed);
		void print(const char * string);
		void print(char c);
		void print(unsigned char b,char * format);
		void print(int n, char * format);
		void print(unsigned int n, char * format);
		void print(long n, char * format);
		void print(unsigned long n, char * format);
	
	
		void println(void);
		void println(const char * string);
		void println(char c);
		void println(unsigned char b,char * format);
		void println(int n, char * format);
		void println(unsigned int n, char * format);
		void println(long n, char * format);
		void println(unsigned long n, char * format);
		void println(double n, char * format);
	
		int available();
		void setTimeout(unsigned long timeout);
		void write(unsigned char c);
		
		
		char read();
		int  readBytes(char * buffer, int number);
	
	private:
		Serial * mbedSerial;
		unsigned long timeout;
		PinName txPin;
		
};
#endif
