#include "mbed.h"
#include "..\Keil_Default\ArduinoSerial.h"
#include "..\Keil_Default\ArduinoSPI.h"
#include "..\Keil_Default\mcp_can.h"

DigitalOut myled(LED1);
DigitalOut myled2(LED2);

DigitalOut			mbedCANSPICS(p8);
InterruptIn			CANInterrupt(p15);
SPI 						mbedCANSPI(p11,p12,p13);
Serial	mbedSerialPC(USBTX, USBRX);
Serial	mbedSerial1(p9,p10);
Serial  mbedSerial2(p28,p27);

ArduinoSerial1 ArduinoSerialPC(&mbedSerialPC, USBTX); 
ArduinoSerial1 ArduinoSerialDisplay(&mbedSerial1, p9);

MCP_CAN folder(p8);

uint8_t packetReceived = 0;

void packetISR()
{
	packetReceived = 1;
	myled2 = !myled2;
}

int main() 
{
	mbedSerialPC.baud(921600);
	
	//Setup
	while (CAN_OK != folder.begin(CAN_500KBPS, MCP_10MHz))
	{
			mbedSerialPC.printf("CAN BUS Init Fail");
	}
	
	mbedSerialPC.printf("Success");
	
	folder.init_Mask(0,1, 0x1FFFFFFF);
	folder.init_Mask(1,1, 0x1FFFFFFF);
	folder.init_Filt(0,1, 0x2001);
	folder.init_Filt(1,1, 0x2003);
	
	folder.init_Filt(2,1, 0x2000);
	folder.init_Filt(3,1, 0x2002);
	folder.init_Filt(4,1, 0x2007);
	
	CANInterrupt.fall(&packetISR);
	
	unsigned char buff[8];
	buff[0] = 0x00;
	buff[1] = 0x01;
	buff[2] = 0x02;
	buff[3] = 0x03;
	buff[4] = 0x04;
	buff[5] = 0x05;
	buff[6] = 0x06;
	buff[7] = 0x07;
	
	//folder.sendMsgBuf(0x2012,1,8,buff);
	
	unsigned char len = 0;

	unsigned char buf[8];
	unsigned long id;
	
    while(1) {
				if (packetReceived == 1)
				{
					 while (CAN_MSGAVAIL == folder.checkReceive()) 

        {

            // read data,  len: data length, buf: data buf
            folder.readMsgBufID(&id,&len, buf);



            // print the data
						mbedSerialPC.printf("%x", id);
						mbedSerialPC.printf("\t");
            for(int i = 0; i<len; i++)

            {

                mbedSerialPC.printf("%x",buf[i]);
							  mbedSerialPC.printf("\t");

            }

            mbedSerialPC.printf("\n\r");
						mbedSerialPC.printf("\n\r");

        }
					
					packetReceived = 0;
				}
			
				//myled2 = !myled2;
				/*byte result = folder.sendMsgBuf(0x2012,1,8,buff);
				if (result == CAN_FAILTX)
				{
					mbedSerialPC.printf("Message Not Sent\n\r");
				}
				else
				{
					mbedSerialPC.printf("Sent Message\n\r");
				}
				*/
    }
		
		/*
		mbedCANSPICS = 1;
		mbedCANSPI.format(8,0);
    mbedCANSPI.frequency(1000000);

		
		while(1)
		{
			mbedCANSPICS = 0;
			//READ INSTRUCTION
			unsigned char firstByte = mbedCANSPI.write(0x04);
			mbedSerialPC.printf("First Byte: %x\n\r", firstByte);
			
			
			unsigned char toWrite = 0x040000;
			unsigned char toRead = 0x000000;
			
			unsigned char msb = mbedCANSPI.write(0x00);
			mbedSerialPC.printf("Second Byte: %x\n\r", msb);
			unsigned char lsb = mbedCANSPI.write(0x00);
			mbedSerialPC.printf("Third Byte: %x\n\r", lsb);
			
			unsigned int value = (msb << 8) | lsb;
			
			mbedSerialPC.printf("ADC 0 value: %d\n\r", value);
			mbedCANSPICS = 1;
			wait(2);
	
		}
	*/
}
